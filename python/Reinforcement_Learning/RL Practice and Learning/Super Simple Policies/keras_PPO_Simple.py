q# -*- coding: utf-8 -*-
"""
Created on Sat Jun 20 15:11:58 2020

@author: elopez8

PPO Model built in Keras taken from:
https://github.com/ChintanTrivedi/rl-bot-football/blob/master/train.py
"""

import numpy as np

#Environment to test on
import gym

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
from tensorflow.keras.applications.mobilenet_v2 import MobileNetV2

clipping_val = 0.2
critic_discount = 0.5
entropy_beta = 0.001
gamma = 0.99 #Comes from GAE paper
lmbda = 0.95 #Comes from GAE paper
# In[Advantages of states via GAE (I think it is GAE)]
def get_advantages(values, masks, rewards): #Hmm, look into generalized advantage estimation
    returns=[]
    gae=0
    for i in reversed(range(len(rewards))):
        delta = rewards[i] + gamma*values[i+1]*masks[i]-values[i]
        gae = delta + gamma * lmbda*masks[i]*gae
        returns.insert(0,gae + values[i])
        
    adv = np.array(returns) - values[:-1]
    return returns, (adv - np.mean(adv)) / (np.std(adv) + 1e-10)
        
# In[Printing the PPO Loss]
def ppo_loss_print(oldpolicy_probs, advantages, rewards, values):
    #Calculates and prints the losses this training episode
    def loss(y_true, y_pred):
        y_true = tf.print(y_true, [y_true], 'y_true: ')
        y_pred = tf.print(y_pred, [y_pred], 'y_pred: ')
        newpolicy_probs = y_pred
        newpolicy_probs = tf.print(newpolicy_probs, [newpolicy_probs], 'new policy probs: ')
        
        ratio = K.exp(K.log(newpolicy_probs + 1e-10) - K.log(oldpolicy_probs+1e-10))
        ratio = tf.print(ratio, [ratio], 'ratio: ')
        p1 = ratio * advantages
        p2 = K.clip(ratio, min_value = 1-clipping_val, max_value=1+clipping_val)*advantages
        actor_loss = -K.mean(K.minimum(p1,p2))
        actor_loss = tf.print(actor_loss, [actor_loss], 'actor_loss: ')
        critic_loss = K.mean(K.square(rewards - values))
        critic_loss = tf.print(critic_loss, [critic_loss], 'critic_loss: ')
        term_a = critic_discount * critic_loss
        term_a = tf.print(term_a, [term_a], 'term_a: ')
        term_b_2 = K.log(newpolicy_probs + 1e-10)
        term_b_2 = tf.print(term_b_2, [term_b_2], 'term_b_2: ')
        term_b = entropy_beta * K.mean(-(newpolicy_probs * term_b_2))
        term_b = tf.print(term_b, [term_b], 'term_b: ')
        total_loss = term_a + actor_loss - term_b
        total_loss = tf.print(total_loss, [total_loss], 'total_loss: ')
        
        return total_loss
    
    return loss

# In[Calculates the Loss, PPO style]
"""
Custom loss function set up in this way to follow Keras API. Look at link below for example.    

https://heartbeat.fritz.ai/how-to-create-a-custom-loss-function-in-keras-637bd312e9ab
"""    

def ppo_loss(oldpolicy_probs, advantages, rewards, values):
    def loss(y_true, y_pred):
        newpolicy_probs = y_pred
        ratio = K.exp(K.log(newpolicy_probs + 1e-10) - K.log(oldpolicy_probs + 1e-10))
        p1 = ratio * advantages
        p2 = K.clip(ratio, min_value = 1-clipping_val, max_value = 1+clipping_val)*advantages
        actor_loss = -K.mean(K.minimum(p1,p2))
        critic_loss = K.mean(K.square(rewards - values))
        total_loss = critic_discount * critic_loss + actor_loss - entropy_beta * K.mean(
            -(newpolicy_probs + K.log(newpolicy_probs+1e-10)))
        return total_loss
    return loss

# In[Create Actor and Critic Neural Networks]
def get_model_actor_simple(input_dims, output_dims):
    state_input = Input(shape=input_dims)
    oldpolicy_probs = Input(shape=(1, output_dims,))
    advantages = Input(shape=(1,1,))
    rewards = Input(shape=(1,1,))
    values = Input(shape=(1,1,))
    
    # Classification Block
    x = Dense(512, activation='relu', name='fc1')(state_input)
    x = Dense(256, activation='relu', name='fc2')(x)
    out_actions = Dense(output_dims, activation = 'softmax', name='predictions')(x)
    
    
    """
    I do not completely understand why you add inputs that are not actually inputs here..
    The documentation for keras does not offer an suggestions or help on this.
    """
    model = Model(inputs=[state_input, oldpolicy_probs, advantages, rewards, values],
                  outputs=[out_actions])

    model.compile(optimizer=Adam(lr=1e-4), loss=[ppo_loss(
        oldpolicy_probs=oldpolicy_probs,
        advantages=advantages,
        rewards=rewards,
        values=values)])
    
    #model.summary() # Uncomment if you want to see
    return model

def get_model_critic_simple(input_dims):
    # Critic, aka the state-value function
    state_input = Input(shape=input_dims)
    
    # Classification block
    x = Dense(512, activation = 'relu', name='fc1')(state_input)
    x = Dense(256, activation='relu', name='fc2')(x)
    out_actions = Dense(1, activation='tanh')(x)
    
    model = Model(inputs=[state_input], outputs=[out_actions])
    model.compile(optimizer=Adam(lr=1e-4), loss='mse')
    
    return model

# In[Test Reward of current Actor]
def test_reward(env):
    """
    Tests the agent in one environmental run
    """
    state = env.reset()
    done = False
    total_reward = 0
    print('testing...')
    limit = 0
    while not done: 
        state_input=K.expand_dims(state, 0)
        action_probs = model_actor.predict([state_input, dummy_n, dummy_1, dummy_1, dummy_1], steps=1)
        # Gonna need fixing up above
        action = np.argmax(action_probs)
        next_state, reward, done, _ = env.step(action)
        state = next_state
        total_reward += reward
        limit += 1
        if limit > 20: #So we test the agent for 20 timesteps.. NO MORE
            break
    return total_reward

def one_hot_encoding(probs):
    one_hot = np.zeros_like(probs)
    one_hot[:, np.argmax(probs, axis=1)] = 1
    return one_hot



# In[Training happens down here]
"""
Let's make the environment and training and all
"""
env = gym.make('CartPole-v0')

state = env.reset()
state_dims = env.observation_space.shape # Shape of the state space
n_actions = env.action_space.n #Number of actions available

dummy_n = np.zeros((1, 1, n_actions))
dummy_1 = np.zeros((1, 1, 1))

tensor_board = TensorBoard()



model_actor = get_model_actor_simple(input_dims = state_dims, output_dims=n_actions)
model_critic = get_model_critic_simple(input_dims=state_dims)

print('Actor Model:')
model_actor.summary()
print()
print('Critic Model:')
model_critic.summary()

"""
ppo_steps: Number of timesteps before a training occurs across the Neural Network.
            If the the episode terminates (if pole falls) then we simply continue into the next time step.
"""
ppo_steps = 128
target_reached = False
best_reward = 0
iters = 0
max_iters = 50

K.clear_session()
tf.config.experimental_run_functions_eagerly(True) #https://github.com/tensorflow/tensorflow/issues/34944

while not target_reached and iters < max_iters:
    states = []
    actions = []
    values = []
    masks = []
    rewards = []
    actions_probs = []
    actions_onehot = []
    state_input = None
    
    for ppo_step in range(ppo_steps):
        state_input = K.expand_dims(state, 0)
        action_dist = model_actor.predict([state_input, dummy_n, dummy_1, dummy_1, dummy_1], batch_size=1, steps=1)
        q_value = model_critic.predict([state_input], steps=1) # Shouldn't this actually be v_value?
        action = np.random.choice(n_actions, p=action_dist[0,:])
        action_onehot = np.zeros(n_actions)
        action_onehot[action] = 1
        
        observation, reward, done, info = env.step(action)
        print('ppo_step: ', str(ppo_step), 
              ', action=', str(action),
              ', reward=', str(reward),
              ', q-value=', str(q_value))
        
        mask = not done
        
        states.append(state)
        actions.append(action)
        actions_onehot.append(action_onehot)
        values.append(q_value)
        masks.append(mask)
        rewards.append(reward)
        actions_probs.append(action_dist)
        
        state = observation
        if done:
            env.reset()
            
    q_value = model_critic.predict(state_input, steps=1)
    values.append(q_value)
    returns, advantages = get_advantages(values, masks, rewards)
    
    # Time to train!
    actor_loss = model_actor.fit(
        [states, actions_probs, advantages, np.reshape(rewards, newshape=(-1, 1, 1)), values[:-1]],
        [(np.reshape(actions_onehot, newshape=(-1, n_actions)))],
        verbose=True,
        shuffle=True,
        epochs=8,
        callbacks=[tensor_board])
    
    critic_loss = model_critic.fit(
        [states], 
        [np.reshape(returns, newshape=(-1, 1))],
        shuffle=True,
        epochs=8,
        verbose=True,
        callbacks=[tensor_board])

    avg_reward = np.mean([test_reward() for _ in range(5)])
    print('Total test reward:', str(avg_reward))
    
    if avg_reward > best_reward:
        print('best reward:', str(avg_reward))
        model_actor.save('model_actor_{}_{}.hdf5'.format(iters, avg_reward))
        model_critic.save('model_critic_{}_{}.hdf5'.format(iters, avg_reward))
        best_reward=avg_reward
        
    if best_reward > 0.9 or iters > max_iters:
        target_reached = True
        
    iters += 1
    env.reset()
    
env.close()