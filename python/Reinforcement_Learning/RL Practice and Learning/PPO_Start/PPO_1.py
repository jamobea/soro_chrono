# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 22:13:24 2020

@author: 17088

Steps that state 'ALG STEP #' refer to the PPO algorithm as defined in the paper.

https://medium.com/analytics-vidhya/coding-ppo-from-scratch-with-pytorch-part-1-4-613dfc1b14c8

The parameter upodate had to be changed slightly, as the PyTorch method was not directly implemented in Tensorflow until v2 and is still being explored. There are three ways to update model parameters in Tensorflow:
    1. Using model.compile() to prepare how your model will learn then using model.fit() to train the model with a certain number of epochs, batch_size, etc. Requires a custom loss function.
    2. Using tf.GradientTape(), the newer way, which acts similar to PyTorch's method. Click here for more information: https://www.pyimagesearch.com/2020/03/23/using-tensorflow-and-gradienttape-to-train-a-keras-model/
    3. (Not sure on this one) Can use optimizer. minimize? Check here for more information: https://www.tensorflow.org/api_docs/python/tf/keras/optimizers/Optimizer#minimize
    
With the implementation of tf.stop_gradient(), take a look at this code for some inspiration: https://scelesticsiva.github.io/2018/01/22/stop-gradients/

Current State:
    - When running with a debugger, the observation continues to return an incorrect shape, but only for the first observation when the environment is reset. This is bad, as the inconsistent shape will produce errors during the back-propgation. Attempted solutions included:
        - Editing just the first observation with [np.newaxis]. Problem with this is the shape of the observation dictates the shape of the action to take, which dictates the shape of the next observation, propogating the issue forwrad
    The next solution to be attempted should be iterating through the batch_obs before it is converted to a tensor in line 148 and changing the shape of any index that is out of line with the rest. I.e. if batch_obs[ind].shape!=(1,3): batch_obs[ind]=batch_obs[ind][np.newaxis].T
"""
from network import FeedForwardNN
import tensorflow as tf
import tensorflow_probability as tfp
import numpy as np
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.backend import mean, std, clip
import pdb # For debugging. Remove when not needed

class PPO:
    def __init__(self, env):
        self.env = env
        self.obs_dim = env.observation_space.shape[0]
        self.act_dim = env.action_space.shape[0]
        
        # ALG STEP #1
        # Initialize Actor and Critic Networks
        self.actor = FeedForwardNN(self.obs_dim, self.act_dim)
        self.critic = FeedForwardNN(self.obs_dim, 1)
        
        self._init_hyperparameters()
        
        # Create our variable for the matrix
        # Note that 0.5 for stdev was chosen arbitrarily
        co_temp=np.full((self.act_dim), 0.5)
        self.cov_var = tf.convert_to_tensor(co_temp, dtype=tf.float32)
        self.cov_mat = tf.linalg.diag(self.cov_var)
        
        self.adam_optim = Adam(learning_rate=self.lr) # Used for both the actor and the critic
    
    def _init_hyperparameters(self):
        # Default values for hypterparameters, will need to change if other parameters are desired.
        self.timesteps_per_batch = 2000       # Timesteps per batch
        self.max_timesteps_per_episode = 1600 # Timesteps per episode
        self.gamma = 0.95                     # Discount factor
        self.n_epochs_per_iteration = 5       # Epochs each update (number of updates per iteration)
        self.clip = 0.2                       # Clipping value for PPO, as recommended for PPO
        self.lr = 0.005                       # Adam optimizer learning rate
        self.num_episodes=0                   # For verbosity
    
    def learn(self, total_timesteps):
        t_so_far = 0 # Timesteps simulated so far
        
        # ALG STEP #2
        while t_so_far < total_timesteps:
            # ALG STTEP #3
            batch_obs, batch_acts, batch_log_probs, batch_rtgs, batch_lens = self.rollout()
            
            # Calculate how many timesteps we collected this batch
            t_so_far += np.sum(batch_lens)
            
            # Calculate V_{phi, k}
            V, _ = self.evaluate(batch_obs, batch_acts)
            
            # ALG STEP 5
            # Calculate Advantage
            A_k = batch_rtgs- tf.stop_gradient(V) # Stop_gradient prevents the critic network from being updated in this coming step. I.e. the gradient of V in A_k will NOT be considered moving forward from here.
            A_k = (A_k - mean(A_k)) / (std(A_k) + 1e-10) # Normalizing the advantage to stablize learning
            
            for _ in range(self.n_epochs_per_iteration):
                with tf.GradientTape() as tape:
                    # Calculate pi_theta( a_t | s_t)
                    _, curr_log_probs = self.evaluate(batch_obs, batch_acts)
                    ratio = tf.math.exp(curr_log_probs - batch_log_probs)
                    
                    # Calculate surrogate losses
                    surr1 = ratio * A_k
                    surr2 = clip(ratio, 1-self.clip, 1+self.clip) * A_k
                    actor_loss = mean(-tf.math.minimum(surr1,surr2))
        
                # Calculate gradients and perform backward propogation for actor network
                grads = tape.gradient(actor_loss, self.actor.trainable_variables)
                print('Updating Agent Network')
                self.adam_optim.apply_gradients(zip(grads, self.actor.trainable_variables))
                
                # Calculate V_phi and pi_theta(a_t | s_t)
                with tf.GradientTape() as V_tape:
                    V, curr_log_probs = self.evaluate(batch_obs, batch_acts)
                    
                # Calculate gradients and perform backward propogation for critic network
                critic_loss = tf.keras.losses.MSE(batch_rtgs, V)
                V_grads = V_tape.gradients(critic_loss, self.critic.trainable_variables)
                print('Updating Value Network')
                self.adam_optim.apply_gradients(zip(V_grads, self.critic.trainable_variables))
                
                
    def rollout(self):
        # Batch Data
        batch_obs = []
        batch_acts = []       # batch observations
        batch_log_probs = []  # log probs of each action
        batch_rews = []       # batch rewards
        batch_rtgs = []       # batch rewards-tp-go
        batch_lens = []       # epsisodic lengths in batch
        
        # Number of timesteps run so far this batch
        t = 0
        while t < self.timesteps_per_batch:
            # Rewards this episode
            ep_rew=[]
            obs = self.env.reset()
            done = False
            
            print('Episode number',self.num_episodes+1)
            self.num_episodes+=1
            for ep_t in range(self.max_timesteps_per_episode):
                #Increment timesteps ran this batch so far
                t+=1
                
                #Collect Observation
                obs=np.reshape(obs,(self.obs_dim,))
                batch_obs.append(obs)
                action, log_prob = self.get_action(obs)
                obs, rew, done, _ = self.env.step(action)
                
                # Quick reshaping of action before storing
                action = np.reshape(action,(self.act_dim,))
                
                # Collect reward, action, and log prob
                ep_rew.append(rew)
                batch_acts.append(action)
                batch_log_probs.append(log_prob)
                
                if done:
                    break
            
            # Track episodic length and rewards
            batch_lens.append(ep_t + 1) # Plus 1 because timestep starts at 0
            batch_rews.append(ep_rew)
            
        # Fix Array sizes
        # batch_obs[0]=batch_obs[0][np.newaxis].T
        """
        Currently the issue exists here. Where the first observation of each episode is a different shape than the remaiining observations.
        """
        
        pdb.set_trace()
        # Reshape data as tensors in the shape specified before returning
        batch_obs = tf.constant(batch_obs,dtype=tf.float32)
        batch_acts = tf.constant(batch_acts, dtype=tf.float32)
        batch_log_probs = tf.constant(batch_log_probs, dtype=tf.float32)
        
        # ALG STEP #4
        batch_rtgs = self.compute_rtgs(batch_rews)
        
        # Return the batch data
        return(batch_obs, batch_acts, batch_log_probs, batch_rtgs, batch_lens)
            
    def compute_rtgs(self, batch_rews):
        # The rewards-to-go (rtg) per episode per batch to return
        # The shape wll be (num timesteps per episode)
        batch_rtgs = []
        
        # Iterate through each episode backwards to maintain same order in batch_rtgs
        for ep_rews in reversed(batch_rews):
            discounted_reward=0 # Discounted reward so far
            for rew in reversed(ep_rews):
                discounted_reward = rew + discounted_reward*self.gamma
                batch_rtgs.insert(0, discounted_reward)
                
        # Convert the rewards-to-go into a tensor
        batch_rtgs=tf.constant(batch_rtgs, dtype=tf.float32)
        return batch_rtgs
                
    def get_action(self, obs):
        # Query the actor network for a mean action.
        # Same thing as calling self.actor.forward(obs)
        mean = self.actor.forward(obs) # This must be a tensor with shape=(act_dim,) to match the cov_mat
        
        # Create Multivariate Normal Distribution
        dist = tfp.distributions.MultivariateNormalFullCovariance(mean, self.cov_mat)
        
        # Sample an action from the distribution and get is log probability
        action = dist.sample()
        log_prob=dist.log_prob(action)
        
        # Return the sampled action and the log probability of that action
        return(action.numpy(), log_prob.numpy())
    
    def evaluate(self, batch_obs, batch_acts):
        # Query critic network for a value V for each obs in batch_obs.
        V = tf.squeeze(self.critic.forward(batch_obs))
        
        # Calculate the log probabilities of batch actions using ost recent actor network
        # This segment of code is similar to that in get_action()
        mean = self.actor.forward(batch_obs)
        dist = tfp.distributions.MultivariateNormalFullCovariance(mean, self.cov_mat)
        log_probs = dist.log_prob(batch_acts)
        return V, log_probs


# %% Testing
        
import gym
env=gym.make('Pendulum-v0')
model = PPO(env)
model.learn(10000)