# -*- coding: utf-8 -*-
"""
Created on Mon Nov  2 22:12:45 2020

@author: 17088

For more information on making a feed-forward netowrk:
    https://keras.io/api/models/model/
Note that the website claims to create the model using 'tf.keras.Model', however tensorflow API states otherwise:
    https://www.tensorflow.org/api_docs/python/tf/keras/Model
"""

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Input
import numpy as np

class FeedForwardNN:
    def __init__(self, input_dim=None, output_dim=None):
        
        self.input_dim=input_dim
        if input_dim==None or output_dim==None:
            raise Warning('Input/Output Dimension cannot be None!')
        model_in = Input(shape=(self.input_dim,))
        layer_1=Dense(64, activation='tanh',name='layer_1')(model_in)
        layer_2=Dense(64, activation='tanh',name='layer_2')(layer_1)
        out_actions=Dense(output_dim, activation='tanh', name='Actions')(layer_2)
        
        self.model = Model(inputs=model_in, outputs=[out_actions])
        print(self.model.summary())
        print()
        
    def forward(self, obs):
        obs=obs[np.newaxis] #Required to ensure shpae is correct
        output = self.model.predict(obs, steps=1)
        return(output)