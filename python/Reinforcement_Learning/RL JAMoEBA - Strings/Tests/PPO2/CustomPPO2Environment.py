# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 09:24:08 2021

@author: elope

For information on custom policies, refer to the following:
    https://stable-baselines.readthedocs.io/en/master/guide/custom_policy.html
    
In general, the 'net_arch' parameter is what is changed to update the custom policy in the following format:
    In short: [<shared layers>, dict(vf=[<non-shared value network layers>], pi=[<non-shared policy network layers>])].
Main difference is there is a call for 'lstm'
"""
# %% Loading Parameters
import os

#Strings Environment
import Strings_Environment as Strings

#Stable Baselines Dependences
from stable_baselines.common.policies import register_policy, LstmPolicy
from stable_baselines.common.policies import MlpPolicy
from stable_baselines import PPO2
from stable_baselines.common import make_vec_env

# %% Training
membrane = 2
gain=2
kwargs = {'mem':2, 'gain':2}

class CustomLSTMPolicy(LstmPolicy):
    def __init__(self, sess, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm=64, reuse=False, **_kwargs):
        super().__init__(sess, ob_space, ac_space, n_env, n_steps, n_batch, n_lstm, reuse,
                         net_arch=[0, 'lstm', dict(vf=[5, 10], pi=[10])],
                         layer_norm=True, feature_extraction="mlp", **_kwargs)

# Register the policy, it will check that the name is not already taken
register_policy('CustomPolicy', CustomLSTMPolicy)

env = make_vec_env(Strings.Strings, n_envs= 4, env_kwargs=kwargs)
model = PPO2('CustomPolicy', env, verbose=1)
model.learn(total_timesteps=25000)
model.save("ppo2_LSTM_Strings")

del model
del env

# %% Reloading 

env = Strings.Strings(mem=2,gain=2)
model = PPO2.load("ppo2_LSTM_Strings")

# Enjoy trained agent
obs = env.reset()
render_time = 1000
for t in range(render_time):
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()
    
env.close()