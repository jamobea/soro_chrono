# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 20:21:30 2021

@author: elopez8

Script to test multiprocessing with Strings_Environment before putting it on the workstation
"""


import numpy as np
#Strings Environment
import Strings_Environment_Small as Strings

# Call back to save function
from stable_baselines.bench import Monitor
from saveBestTrainingCallback import SaveOnBestTrainingRewardCallback
from stable_baselines import PPO1

from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common.vec_env import SubprocVecEnv
import os

savefile = 'C:/parallelRLtest/'
os.makedirs(savefile,exist_ok=True)

callback = SaveOnBestTrainingRewardCallback(check_freq = 100, 
                                            log_dir = savefile, 
                                            num_ep_save = 5)

if __name__ == '__main__':
    # env = SubprocVecEnv([lambda: Monitor(Strings.Strings(mem=2,gain=2), savefile) for _ in range(2)])
    env = Monitor(Strings.Strings(mem=2,gain=2), savefile)
    model = PPO1(MlpPolicy,env)
    model.learn(total_timesteps = 10000, callback = callback)
    model.save(savefile+'agent_after_training')