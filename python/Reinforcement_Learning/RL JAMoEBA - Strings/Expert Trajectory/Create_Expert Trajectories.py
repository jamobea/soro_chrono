# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 09:04:31 2020

@author: elopez8

Generating Expert Trajectories
"""
# %% Import Dependencies

from stable_baselines.gail import generate_expert_traj
from Control import Control
import Strings_Environment_Large as Strings
import numpy as np
import os

# %% Prepare Environment

name='Control_gapPush_Large_2021_01_06'
env = Strings.Strings(experiment_name=name, mem=2, gain=2)

new_folder = './'
os.makedirs(new_folder, exist_ok=True)

# %% Create Expert Trajectory
_ = env.reset()

forcex = 1  # Force applied in the x-direction
forcez= 1 # Force applied in the z-direction

control=Control(forcex, forcez, env)

file = new_folder + name

generate_expert_traj(control.gap_push, file, env, 40000, 5)