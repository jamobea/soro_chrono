# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:20:55 2020

@author: elopez8
"""

# Must now be executed

if __name__ == '__main__':
    # In[Import Dependencies]
    import os
    import pathlib
    from shutil import copyfile
    import matplotlib.pyplot as plt
    
    #Strings Environment
    import Strings_Environment_Large as Strings
    from saveBestTrainingCallback import SaveOnBestTrainingRewardCallback
    
    #Stable Baselines Dependences
    from stable_baselines.common.policies import register_policy, FeedForwardPolicy
    from stable_baselines import PPO2
    from stable_baselines.common import make_vec_env
    from stable_baselines.common.vec_env import SubprocVecEnv
    from stable_baselines.gail import ExpertDataset
    from datetime import date
    import time
    
    # In[Prepare Experiment Parameters]
    
    experiment_num=165
    
    #Pre-Training batchsize
    pretrain_batchsize=200
    pretraining_epochs = 1000 # Number of epochs for pretraining
    
    # Training Parameters
    neural_network =[500,250,100,60]
    policyName = 'CustomPolicy'
    training_timesteps = 1_000_000
    gamma = 0.99            # Discount factor
    n_steps = 5000          # Number of steps to run in each environment per update. Batchsize = n_steps*n_env
    ent_coef = 0.01         # Entropy coefficient
    learning_rate = 0.00025 # Learning Rate, can be a funcion
    vf_coef = 0             # Value Function Coefficient in Loss Function
    max_grad_norm = 0.5     # Clipping factor for gradients. Should prevent exploding gradients
    lam = 0.95              # Factor for bias vs variance for GAE
    nminibatches = 8        # Number of minibatches at each update Thus the minibatchsize = batchsize//minibatches
    noptepochs = 4          # Number of epochs each update
    cliprange = 0.2         # Cliprange for PPO
    seed = 12345            # Seed for neuralnetwork initialization
    
    # Environment Parameters
    mem = 2
    gain = 2
    kwargs = {'mem':mem, 'gain':gain}
    experiment_name = "Experiment_{}".format(str(experiment_num))
    
    # Parameters for Calback
    num_ep_save = 2 # Calculate the mean reward for this number of episodes and save that model
    check_freq = 50000 # After how many timesteps do we check the frequency
    
    #Test after training
    test=True
    num_tests=3
    data_collect=True
    plot=True
    render=True
    POV_Ray=True
    num_tests=3
    time_per_test=20000
    
    pretraining = True
    pretrain2 = 'Control_10_Large_2021_01_05.npz' # Even if pretraining is FALSE, there still must be a string here
    if pretraining:
        #Esteban's Desktop
        # pretrain = 'C:/Users/elope/Documents/Repositories/Soro-Chrono/python/Reinforcement_Learning/RL JAMoEBA - Strings/Expert Trajectory/'
        
        # Big Gucci
        pretrain = 'C:/soro_bitbucket/python/Reinforcement_Learning/RL JAMoEBA - Strings/Expert Trajectory/'
        
        pretrain += pretrain2
        dataset = ExpertDataset(pretrain, traj_limitation=1, batch_size=pretrain_batchsize)
        
    #Save location
    current = str(pathlib.Path(__file__).parent.absolute())
    savefile=current+'\\Experiment {} {}\\'.format(str(experiment_num), date.today())
    os.makedirs(savefile, exist_ok=True)
    
    # In[Create Tensorboard Directories, Neural Network and Train]
        
    # Custom MLP policy and register it
    class CustomPolicy(FeedForwardPolicy):
        def __init__(self, *args, **kwargs):
            super(CustomPolicy, self).__init__(*args, **kwargs,
                                                net_arch=[dict(pi=neural_network,
                                                              vf=neural_network)],
                                                feature_extraction="mlp")

    register_policy(policyName, CustomPolicy)
 
    # Setting up Callback
    callback = SaveOnBestTrainingRewardCallback(check_freq = check_freq, log_dir = savefile, num_ep_save = num_ep_save)
    
    sub_env = Strings.Strings(mem=mem,gain=gain)
 
    # Creating multiple environments for multiple workers
    env = make_vec_env(Strings.Strings, n_envs= 4, env_kwargs=kwargs, vec_env_cls=SubprocVecEnv, monitor_dir = savefile)
    new_env='Strings_Environment_Experiment_{}'.format(str(experiment_num))
    copyfile(Strings.__file__,savefile+'Strings_Environment_Experiment_{}.py'.format(str(experiment_num)))

    # Create and pretrain the model with expert trajectories
    model = PPO2(policyName, env, verbose=1, 
                 gamma=gamma, 
                 n_steps = n_steps, 
                 ent_coef = ent_coef,
                 learning_rate = learning_rate,
                 vf_coef = vf_coef,
                 nminibatches = nminibatches,
                 noptepochs = noptepochs,
                 cliprange = cliprange,
                 tensorboard_log = savefile,
                 seed = seed)
    
    if pretraining:
        pre_train_start = time.time()
        model.pretrain(dataset, n_epochs=pretraining_epochs)
        pre_train_end = time.time()
    
    learn_start = time.time()
    model.learn(total_timesteps=training_timesteps, callback=callback)
    learn_end = time.time()
    
    model.save(savefile+'ppo2_Strings_' + experiment_name)
    sub_env.parameter_export()
    
    RL_Training_Parameters=[['Experiment Name:',experiment_name,
                            ['Neural Network:',str(neural_network)],
                            ['Training Timesteps:',str(training_timesteps)],
                            ['Timesteps Per Environmant Per Update:', str(n_steps)],
                            ['Num of Minibatches:',str(nminibatches)],
                            ['Epochs Per Update:',str(noptepochs)],
                            ['Gamma:',str(gamma)],
                            ['Pretrain Batchsize:',str(pretrain_batchsize)],
                            ['Pretrain Epochs:',str(pretraining_epochs)],
                            ['Entropy Coefficient:',str(ent_coef)],
                            ['Learning Rate:', str(learning_rate)],
                            ['Value Function Coeff:',str(vf_coef)],
                            ['Time for Pretraining:',str(round(pre_train_end - pre_train_start,2))],
                            ['Time for Learning:',str(round(learn_end - learn_start,2))],
                            ['Seed:',str(seed)],
                            ['Pretraining Present?:',str(pretraining)],
                            ['Pretraning npz:', pretrain2]]]
    
    txt_file=savefile+'RL Training Paremeters.txt'
    with open(txt_file, 'w') as f:
        for line in RL_Training_Parameters:
            f.write("%s\n" % line)
    
    del model
    del env
    del sub_env
        
    # %%Model Evaluation - Test the Agent
    """
    Currently only works when testing with one made model. Not ready for multiple ANN tests.
    """
    if test:
        os.chdir(savefile)
        Env_import=__import__(new_env)
        model = PPO2.load(savefile+'ppo2_Strings' + experiment_name+'.zip')
        
        for i in range(num_tests):
            name=experiment_name+'_v{}'.format(str(i+1))
            Env=Env_import.Strings(data_collect=data_collect, experiment_name=name, plot=plot, POV_Ray = POV_Ray, mem=mem, gain=gain)
            obs=Env.reset()
            
            for j in range(time_per_test):
                action, _states = model.predict(obs)
                obs, reward, done, info = Env.step(action)
            
                if render: Env.render()
                
                if i%10==0: print('Time Step:',j)
                    
                if done: break
        
            if data_collect: Env.data_export() # Export the data from the simulation
            plt.close('all')
            Env.close()