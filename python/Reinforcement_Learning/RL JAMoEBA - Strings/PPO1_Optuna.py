# -*- coding: utf-8 -*-
"""
Created on Mon Jan 19 22:20:55 2021

@author: elopez8
"""

# %% Import Dependenciesq    
import os
import site
from datetime import date 
from shutil import copyfile
import matplotlib.pyplot as plt
import pdb
import numpy as np

import optuna
import joblib

"""
To avoid having to copy and paste the desired environment into the site packages everytime there is a modeifcation made, the below process ensures that the environment of interest is copied directly into the site-packages directory before import.
"""
import Strings_Environment_SmallShortAction as Strings1
import saveBestTrainingCallback as SavingCallback
os.makedirs(site.getsitepackages()[1]+'\\ELRL\\', exist_ok=True)
copyfile(Strings1.__file__,site.getsitepackages()[1]+'\\ELRL\\Strings_Environment.py')
copyfile(SavingCallback.__file__, site.getsitepackages()[1]+'\\ELRL\\saveBestTrainingCallback.py')

#Strings Environment
import ELRL.Strings_Environment as Strings

# Callback to save the best learned model in the background
from stable_baselines.bench import Monitor
from ELRL.saveBestTrainingCallback import SaveOnBestTrainingRewardCallback

#Stable Baselines Dependences
from stable_baselines.common.policies import FeedForwardPolicy, register_policy
from stable_baselines import PPO1

# %% Prepare Experiment Parameters

experiment_num = 163

import pathlib
current = str(pathlib.Path(__file__).parent.absolute())
savefile=current+'\\Experiment {} {}\\'.format(str(experiment_num), date.today())
os.makedirs(savefile, exist_ok=True)

def objective(trial):

    global experiment_num

    #Pre-Training batchsize
    pretrain_batchsize=200
    pretrain_epochs = 1000 # Number of epochs for pretraining
    
    # Training Parameters
    # nns=[[500,250,100,60]]
    num_layers = trial.suggest_int('Num Layers', 3, 6)
    neural_network = []
    for layer in range(num_layers):
        neural_network.append(trial.suggest_int('layer_{}'.format(layer),100,2000))
    policy_name='Policy_{}'.format(trial.number) # There must be a name for each NN
    training_timesteps = 1000000
    timesteps_per_actorbatch = trial.suggest_int('timesteps_per_actorbatch',100,20000)
    gamma = trial.suggest_float('Gamma', 0.9, 1) # Discount Factor
    batchsize = trial.suggest_int('batchsize', 50, 1000)
    epochs = trial.suggest_int('eopchs', 2, 10)
    entcoeff = 0
    schedule= trial.suggest_categorical('LearningRateScheduler',['linear','constant'])
    seed=12345
    
    # Environment Parameters
    gain=2
    membrane=2
    experiment_name = "Experiment_{}".format(str(experiment_num))
    
    # Parameters for Calback
    num_ep_save = 2 # Calculate the mean reward for this number of episodes and save that model
    check_freq = 50000 # After how many timesteps do we check the frequency
    
    
    #Save location
    global current
    global savefile
    
    # Sub folder for trial
    subFolder = savefile + 'Trial_{}\\'.format(trial.number)
    os.makedirs(subFolder, exist_ok = True)
    
    #Test after training
    test=True
    num_tests=5
    data_collect=True
    plot=True
    render=True
    POV_Ray=True
    time_per_test=20000
    
    # %% Create Tensorboard Directories, Neural Network and Train
    
    tensorboard_log = subFolder + experiment_name + " Tensorboard Log/"
    os.makedirs(tensorboard_log,exist_ok=True)
        
    # Custom MLP policy of three layers of size 'n' each
    class CustomPolicy(FeedForwardPolicy):
        def __init__(self, *args, **kwargs):
            super(CustomPolicy, self).__init__(*args, **kwargs,
                                                net_arch=[dict(pi=neural_network,
                                                              vf=neural_network)],
                                                feature_extraction="mlp")
    
    # Register the policy, it will check that the name is not already taken
    register_policy(policy_name, CustomPolicy)
    
    # For callback
    env = Monitor(Strings.Strings(experiment_name=subFolder + experiment_name, mem=membrane, gain=gain), subFolder)
    callback = SaveOnBestTrainingRewardCallback(check_freq = check_freq, log_dir = subFolder, num_ep_save = num_ep_save)
    
    new_env='Strings_Environment_Experiment_{}'.format(str(experiment_num))
    copyfile(Strings.__file__,savefile+new_env+'.py')
    # Create and pretrain the model with expert trajectories
    model = PPO1(policy_name, env, verbose=1, optim_batchsize=batchsize, tensorboard_log=tensorboard_log, gamma=gamma, timesteps_per_actorbatch=timesteps_per_actorbatch, seed=seed, optim_epochs=epochs, entcoeff=entcoeff, schedule=schedule)
        
    model.learn(total_timesteps=training_timesteps, callback=callback)
    model.save(subFolder+'ppo1_Strings_' + experiment_name+'_Trial{}'.format(trial.number))
    
    env.parameter_export()
    env.close()
    
    RL_Training_Parameters=[['Experiment Name:',experiment_name],
                            ['Neural Network:',str(neural_network)],
                            ['Training Timesteps:',str(training_timesteps)],
                            ['Timesteps Per Actor Update:', str(timesteps_per_actorbatch)],
                            ['Batchsize Per Update:',str(batchsize)],
                            ['Epochs Per Update:',str(epochs)],
                            ['Gamma:',str(gamma)],
                            ['Pretrain Batchsize:',str(pretrain_batchsize)],
                            ['Pretrain Epochs:',str(pretrain_epochs)],
                            ['Entropy Coefficient:',str(entcoeff)],
                            ['Learning Rate Scheduler:', schedule],
                            ['Seed:',str(seed)]]
    
    
    txt_file = subFolder + 'RL Training Paremeters Trial{}.txt'.format(str(trial.number))
    with open(txt_file, 'w') as f:
        for line in RL_Training_Parameters:
            f.write("%s\n" % line)
    del model
    del env
        
    # %% Model Evaluation - Test the Agent
    if test:
        
        final_distances = np.zeros(num_tests)
        
        os.chdir(savefile)
        Env_import=__import__(new_env)
        model = PPO1.load(subFolder+'ppo1_Strings_' + experiment_name+'_Trial{}.zip'.format(trial.number))
        
        for i in range(num_tests):
            name=subFolder + experiment_name+'_v{}'.format(str(i+1))
            Env=Env_import.Strings(data_collect=data_collect, experiment_name=name, plot=plot, POV_Ray = POV_Ray, mem=membrane, gain=gain)
            obs=Env.reset()
            
            for j in range(time_per_test):
                action, _states = model.predict(obs)
                obs, reward, done, info = Env.step(action)
            
                if render: Env.render()
                
                if i%10==0: print('Time Step:',j)
                    
                if done: break
            
            bot_pos=obs[0:2*Env.num_bots]
            z_pos=[]
            x_pos=[]
            for i in range(len(bot_pos)):
                if i%2 != 0: #Gathering the z_positions
                    z_pos.append(bot_pos[i])
                elif i%2==0:
                    x_pos.append(bot_pos[i])
            
            average_x = np.mean(x_pos)
            average_z = np.mean(z_pos)
            
            final_distances[i] = np.linalg.norm([average_x,average_z])
            
            if data_collect: Env.data_export() # Export the data from the simulation
            plt.close('all')
            Env.close()
            
    del model
    del Env
    
    return np.mean(final_distances)

study = optuna.create_study()
study.optimize(objective, 10)
joblib.dump(savefile+'optunaStudy.pkl')