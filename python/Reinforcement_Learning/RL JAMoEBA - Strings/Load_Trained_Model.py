# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:39:29 2020

@author: 17088
"""

from stable_baselines import PPO1
import Strings_Environment_Large as Strings
import matplotlib.pyplot as plt

data_collect = True # Collects data and reports it in .csv files for later investigation.
plot = True # Plots your data immediately. Will NOT work if data_collect=False
render = True # Do you want to see the simulation?
POV_Ray = True

tests=3
experiment_num = 157

# model = PPO1.load('ppo1_Strings_CustomPolicy_Experiment_{}.zip'.format(str(experiment_num)))
model = PPO1.load('Pretrain_Test_Experiment_157.zip')

for i in range(tests):
    experiment_name = 'Experiment_{}_v{}'.format(str(experiment_num), str(i+1))
    env = Strings.Strings(data_collect=data_collect, experiment_name=experiment_name, plot=plot, POV_Ray = POV_Ray, mem=2, gain=2)
    obs=env.reset()
    
    for i in range(20000):
        action, _states = model.predict(obs)
        obs, reward, done, info = env.step(action)
        
        if render:
            env.render()
        
        if i%10==0:
            print('Time Step:',i)
        
        if done:
            break
    
    if data_collect:
        env.data_export() # Export the data from the simulation
    plt.close('all')
    env.close()