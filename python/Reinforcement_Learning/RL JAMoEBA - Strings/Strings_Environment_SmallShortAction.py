# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:27:48 2020

email: elopez8@hawk.iit.edu
@author: Esteban Lopez
date: 3/5/2020

https://stable-baselines.readthedocs.io/en/master/guide/rl_tips.html
"""
import pychrono as chrono
try:
    from pychrono import irrlicht as chronoirr
except:
    print('Could not import ChronoIrrlicht')
import pychrono.postprocess as postprocess
import numpy as np
from numpy import mean
from gym import spaces, Env
from math import floor
import os
from datetime import datetime
import matplotlib.pyplot as plt


class Strings(Env):   
    def __init__(self, experiment_name = 'NOT NAMED', data_collect = False, plot = False, POV_Ray=False, mem=0, gain=2.0):
        #-----------------------------
        #     Create the System
        #### Simulation parameters
        #-----------------------------
        self.system = chrono.ChSystemNSC()
        self.time = 0        # Using this to keep track of what is going on and when. Used for plotting.
        
        if mem==0:
            self.timestep=0.01
        elif mem==1: 
            self.timestep=.001
        elif mem==2:
            self.timestep= 0.005
        elif mem==3:
            self.timestep=0.001
        elif mem==4:
            self.timestep=0.001
            
        chrono.ChCollisionModel.SetDefaultSuggestedEnvelope(0.005)
        chrono.ChCollisionModel.SetDefaultSuggestedMargin(0.001)
        self.system.Set_G_acc(chrono.ChVectorD(0,-9.81,0))
        self.system.SetSolverType(chrono.ChSolver.Type_PSSOR)
        self.system.SetSolverMaxIterations(150) # Increase this if issues persists
        
        self.episode = 0                 # Number of episodes completed thus far.
        
        self.info = {"timeout": 10000.0} #some random bullshit
        
        #-----------------------------------
        #### Training and Reward Parameters
        #-----------------------------------
        self.Rew_Constant=10
        self.max_num_timestep=3000 #For reward processing
        self.steps_to_death = 5000 # Number of timesteps allowed before ending simulation
        self.kill_time = self.steps_to_death*self.timestep
        
        self.river_force=2
        
        #-----------------------------------
        #### Bot, In, and Spring Parameters
        #-----------------------------------
        self.num_bots= 30                                  # number of robots
        self.diameter = 0.038                              # Diameter of robots and interior
        self.mass = 0.1                                    # Mass of robots
        self.mass_in = .003                                # Mass of interior
        self.height = 0.06                                 # Height of cylinder
        self.speed_limit=2                                 # Speed limit
        self.rl = 0.000                                    # Resting Length
        self.spring_max = 0.05                             # If springs get this long, they will stiffen to self.k_stiff to avoid interior spilling out
        self.volume = np.pi*.25*self.height*(self.diameter)**2
        self.rho_bot=self.mass/self.volume
        self.rho_in = self.mass_in/self.volume
        
        #----------------------------------
        #### Setting Radius of JAMoEBA
        #----------------------------------
        # Since the dimensions of the environment is heavily decided by the sizes of JAMoEBA, this part is heavily regulated
 
        if mem==0: # no membrane
            self.R=(self.diameter*self.num_bots/(np.pi*2))+.1 # Radius of rings of bots + 10cm
            self.k = 100                                       # Spring constant (bots)
            self.k_stiff = self.k*5                            # If the spring is extended too far, then the stiffness will increase by a factor of 10
            self.b = self.k/50                                 # Damping (bots)
            self.ratioM=0
            
            
        #--------------------------------------------
        #### Skin Particle Paramaters (Membrane)
        #--------------------------------------------        
        elif mem in range(1,5): # Membrane made of skin particles
            self.R = 0.8            # Make the diameter a fixed value
            self.skind = 0.015      # Diameter of skin particles
            self.ratioM = 7         # Ratio of membrane skin particles to big bots
            self.skinrho = 1000      # Density of skin particles [kg/m^3]

        if mem==1 or mem==3 or mem==4:
            self.k = 200           # Spring Constant (skin membrane)
            self.b = 0             # Damping Constant (skin membrane)
            self.k_stiff=1000
            self.spring_max=0.015
            
        if mem==2:
            self.k = 0
            self.b = 0
            self.ratioM=3
            self.skinrho = 500
            
        self.mem=mem            
        self.num_skin = self.ratioM*self.num_bots # Number of skin particles
        self.gain = gain #Multiplies recommended value of neural network by this amount and applies that force to the bot in specified direction.
        self.obstacle_radius = 2*self.R
        
        
        #---------------------------------------
        #### Target position for COG of JAMoEBA
        #---------------------------------------
        self.X_targ = 24*self.R # Distance from JAMoEBA at start
        self.Z_targ = 0
        self.d_old = np.linalg.norm(self.X_targ + self.Z_targ)
        self.start_dis=np.linalg.norm(self.X_targ + self.Z_targ)
        
        #----------------------------------------------
        #### Interior granulars
        #----------------------------------------------
        
        if self.mem == 0: # If there is no membrane, make the interiors as you normally would
            self.in_rings_radius = []                      # Radius of interior rings
            self.gran_per_ring = []
            current_circumference = 2*np.pi*self.R
            current_radius = self.R
            while current_circumference > (self.diameter*3): # Not allowing an interior ring with less than 3 granulars
                current_radius = current_radius - (self.diameter + 0.05) # A 5 centimeter buffer is thrown in to keep add space
                current_circumference = 2*np.pi*current_radius
                self.in_rings_radius.append(current_radius)
                
            if current_circumference > np.pi*self.diameter:
                self.in_rings_radius.append(0)             # If the space allows it, let's throw one more interior in the center
                
            for radius in self.in_rings_radius:
                current_num_interior = floor((2*np.pi*radius)/self.diameter) # Calcualates how many interior granules fit in each interior ring
                self.gran_per_ring.append(current_num_interior)
                
                if radius == 0:
                    self.gran_per_ring.append(1) #Throw in a center granule
            
            for num in self.in_rings_radius:
                if num < 0:
                    self.in_rings_radius.remove(num)
                    
            for num in self.gran_per_ring:
                if num < 0:
                    self.gran_per_ring.remove(num)
    
            self.num_interior = sum(self.gran_per_ring)
            
        if self.mem in range(1,5): # If there IS a membrane, just copy Declan and Qiyuan's code
            self.n=np.arange(self.num_bots+30,5,-7)   # array of interior robots
            
            if self.mem==2: # Removing a few interiors for mem==2
                self.n=self.n[:-3]
                self.n[-1] -= 10
            self.num_interior = np.sum(self.n)
        
        #-------------------------
        #### Floor Parameters
        #-------------------------
        self.length=500 # Floor length and width
        self.tall=0.1   # Floor height

        #----------------------------
        #### Material
        #----------------------------
        self.mu_f = 0.01     # Friction
        self.mu_b = 0.01    # Damping
        """
        The following two friction values have been removed
        self.mu_r = 0.001     # Rolling Friction
        self.mu_s = 0.001     # Spinning Friction
        """

        self.Ct = 0.0001
        self.C = 0.0001
        self.Cr = 0.0001
        self.Cs = 0.0001
        
        # self.material = Material(self.mu_f, self.mu_b, self.mu_r, self.mu_s, self.C, self.Ct, self.Cr, self.Cs)
        self.material = Material(self.mu_f, self.mu_b, self.C, self.Ct, self.Cr, self.Cs) # Removed the rolling and spinning friction
        self.river_material=Material(self.mu_f*2, self.mu_b*0.5, self.C, self.Ct, self.Cr, self.Cs)

        
        #----------------------------
        #### Gym API
        #----------------------------
        #ChronoBaseEnv.__init__(self)
        self.render_setup = False

        self.state_size = self.num_bots*8 + 1# + self.num_interior*4 # Currently not looking at interior data
        # State is: [Bot_Xpos, Bot_Zpos, Bot_Xvel, Bot_Zvel, Bot_action_x, Bot_action_z, Bot_Ext_forceX, Bot_Ext_ForceZ] + system time 
        self.action_size = 20
        # Action is: [3*Bot_force_x, 3*Bot_force_z] for each bot
        
        self.half_ac_size = self.action_size//2
        
        low = np.full(self.state_size, -1000)
        high = np.full(self.state_size, 1000)
        self.observation_space = spaces.Box(low, high, dtype=np.float32)

        #Change the number in 'shape(x,)' where x is the number of actions needed. In this case, 6 (2 forces per self.bot)
        self.action_space = spaces.Box(low=-1.0, high=1.0, shape=(self.action_size,), dtype=np.float32) #Recommended to make the action space units!
        
        #-----------------------------------------
        #### Data Collection Matrices
        #-----------------------------------------
        self.data_collect = data_collect
        self.plot = plot
        self.experiment_name = experiment_name
        self.POV_Ray = POV_Ray
        
        self.environment_parameters = [['X_Targ:',str(self.X_targ)], 
                                       ['Z_targ:', str(self.Z_targ)], 
                                       ['Num_Bots:', str(self.num_bots)],
                                       ['Num_Interior:', str(self.num_interior)],
                                       ['Bot_Diameter:', str(self.diameter)],
                                       ['JAMoEBA_Radius:', str(self.R)], 
                                       ['Bot_Height:',str(self.height)], 
                                       ['Spring_k:', str(self.k)], 
                                       ['Spring_b:', str(self.b)], 
                                       ['Spring_rl:', str(self.rl)], 
                                       ['Force_Gain:', str(self.gain)],
                                       ['Membrane_Type:', str(self.mem)]]
        
        if self.data_collect:
            now=str(datetime.now())
            now=now.replace(":","")
            now=now[:-7]
    
            self.new_folder = experiment_name + ' Data and Plots ' + now + "/"
            
            os.makedirs(self.new_folder, exist_ok=True)
            
            # This +1 is for the extra column needed to record time.
            self.X_data = np.zeros(self.num_bots + 1)
            self.X_vel_data = np.zeros(self.num_bots + 1)
            self.Y_data = np.zeros(self.num_bots + 1)
            self.Y_vel_data = np.zeros(self.num_bots + 1)
            self.Z_data = np.zeros(self.num_bots + 1)
            self.Z_vel_data = np.zeros(self.num_bots + 1)
            self.force_data = np.zeros(self.num_bots*2 + 1)
            self.ac = np.zeros(self.action_size + 1)
            self.reward_data = np.zeros(2)
            self.obs_data = np.zeros(self.state_size +1)
            self.contact_forces = np.zeros(self.num_bots*2 +1)
            self.effort = np.zeros(2)
            
    def reset(self):
        """
        Reset Function - Gets called first, sets up the system
        """
        self.isdone = False
        self.system.Clear()
        self.episode+=1 #Add an episode number to the episode
        
        print('Episode Spawn Number:', self.episode)

        #---------------------------------------
        #### Empty vectors for storing objects
        #---------------------------------------
        self.bots = []         # Store bots!
        self.interior = []     # Store interior granules
        self.forcesX = []      # Store the X force objects!
        self.forcesZ = []      # Store the Z force objects!
        self.Springs = []      # Store the spring. Nothing done with this yet.
        self.obstacles = []    # Store the obstacles, or whatever.
        self.obstacle_ids = [] # Store the IDs of obstacles
        self.skinM = []        # Store skin particles for membrane

        self.skin_posX = []    # Store skin X-pos
        self.skin_posZ = []    # Store the skin Z-pos
        
        self.bound_force = []  # Forces applied to keep interiors inside

        self.X_Positions = np.zeros((20,self.num_bots))
        self.Z_Positions = np.zeros((20,self.num_bots))
        self.X_Actions = np.zeros((20,self.num_bots))
        self.Z_Actions = np.zeros((20,self.num_bots))
        self.tot_power = 0
        self.midway = self.X_Positions.shape[0]//2
        
        self.my_rep = MyReportContactCallback() # Will be used for collision detection
        
        #-------------------------------------------
        #   Storing Moving Averages for observation
        #-------------------------------------------
        self.Avg_PosX = np.zeros([self.num_bots,10]) # Matrix for storing moving averages
        self.Avg_PosZ = np.zeros([self.num_bots,10])
        
        self.Avg_VelX = np.zeros([self.num_bots,10])
        self.Avg_VelZ = np.zeros([self.num_bots,10])
        
        self.Avg_ForceX = np.zeros([self.num_bots,10])
        self.Avg_ForceZ = np.zeros([self.num_bots,10])

        #------------------------
        #### Create the Floor
        #------------------------
        # Floor for general simulation
        self.body_floor = Floor(self.material, self.length, self.tall, r=.827,g=.827,b=.827)
        self.body_floor.SetId(0)
        self.system.Add(self.body_floor)
        
        # Floor for river-crossing simulation
        # Make the first floor with target the self.body_floor for mating
        width = 12*self.R
        # self.body_floor = Floor(self.material, 4*self.R, self.tall, width)
        # floor_2 = Floor(self.material, 4*self.R, self.tall, width, x_pos=-self.X_targ)
        
            #### River floor
        river_floor = Floor(self.material, 8*self.R, self.tall+0.01, width, x_pos = -12*self.R, r=0.678, g=0.847,b=0.902)
        river_floor.SetCollide(False)
        # self.system.Add(self.body_floor)
        # self.system.Add(floor_2)
        # self.system.Add(river_floor)

        #---------------------------
        #### Make the bots!
        #---------------------------
        for i in range(self.num_bots): 
            theta=i*2*np.pi/self.num_bots
            x = self.R*np.cos(theta) - self.X_targ # Translating it so target is at origin.
            y = 0.5*self.height
            z = self.R*np.sin(theta) - self.Z_targ
            
            # Create bots
            self.bot = chrono.ChBody()
            if self.mem==0:
                self.bot = chrono.ChBodyEasyCylinder(self.diameter/2, self.height, self.rho_bot, True, True)
            elif self.mem in range(1,5):
                self.bot = chrono.ChBodyEasyCylinder(self.diameter, self.height, self.rho_bot, True, True)
            self.bot.SetPos(chrono.ChVectorD(x,y,z))
            self.bot.SetMaterialSurface(self.material)
            
            # rotate them
            rotation1 = chrono.ChQuaternionD()
            rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
            self.bot.SetRot(rotation1)

        #----------------------
        #     Don't let tip
        #----------------------

            pt=chrono.ChLinkMatePlane()
            pt.Initialize(self.body_floor,self.bot,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
            self.system.AddLink(pt)

        #---------------------------------
        #       Apply forces to bots
        #---------------------------------

            # X-Direction Force
            self.forcex = chrono.ChForce()
            self.bot.AddForce(self.forcex)
            self.forcex.SetMode(chrono.ChForce.FORCE)
            self.forcex.SetDir(chrono.VECT_X)
            self.forcex.SetVrelpoint(chrono.ChVectorD(0,0,0)) # Force acts on the center of the bots
            self.forcesX.append(self.forcex) # Add the X-Force into the forces array

            # Z-Direction Force
            self.forcez=chrono.ChForce()
            self.bot.AddForce(self.forcez)
            self.forcez.SetMode(chrono.ChForce.FORCE)
            self.forcez.SetDir(chrono.VECT_Z)
            self.forcez.SetVrelpoint(chrono.ChVectorD(0,0,0)) # Force acts on the center of the bots
            self.forcesZ.append(self.forcez) # Add the Z-Force into the forces array
            
            # Set speed limit
            self.bot.SetMaxSpeed(self.speed_limit)
            self.bot.SetLimitSpeed(True)
            
            bot_col = self.chrono_color('purple')
            # col_y.SetColor(chrono.ChColor())
            self.bot.AddAsset(bot_col)
            
            self.bot.SetId(i+1) # Bots have an ID starting from 1
            
            # Set Collision
            self.bot.SetCollide(True)
            self.bot.SetBodyFixed(False)

            
        #---------------------------
        # Attach Springs & Skin
        #---------------------------    
            col_b = chrono.ChColorAsset()
            col_b.SetColor(chrono.ChColor(0,0,1)) # Blue Colors
            if self.mem==0: # Will NOT add anti-escape partiles. AKA no skin.
                
                p1=0
                p2=self.diameter/2
                p3=0
                p4=-self.diameter/2
                h=0 
                    
                if i>=1:
                    # Link Spring
                    self.spring=chrono.ChLinkSpring()
                    self.spring.Initialize(self.bots[i-1], self.bot,True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
                    self.spring.Set_SpringK(self.k)
                    self.spring.Set_SpringR(self.b)
                    self.spring.Set_SpringRestLength(self.rl)
                    self.spring.AddAsset(col_b)
                    self.spring.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                    self.system.AddLink(self.spring)
                    self.Springs.append(self.spring)
                    
                # Last Spring
                if i==self.num_bots-1:
                    self.spring=chrono.ChLinkSpring()
                    self.spring.Initialize(self.bot, self.bots[0], True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
                    self.spring.Set_SpringK(self.k)
                    self.spring.Set_SpringR(self.b)
                    self.spring.Set_SpringRestLength(self.rl)
                    self.spring.AddAsset(col_b)
                    self.spring.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                    self.system.AddLink(self.spring)
                    self.Springs.append(self.spring)
                    
                    
            #-----------------------------------------------
            #       Add bots to system and self.bots array
            #-----------------------------------------------
                    
            #TODO: Remove this later, this is for testing purposes
            col_grey = self.chrono_color('grey')
            
            if i==0 or i==7 or i==15 or i==22:
                # First, eigth, and 14th Bot Grey
                self.bot.AddAsset(col_grey)
                
            
            #Uncomment if you want to see the second bot as pink
            # if i==1:
            #     # Second Bot Pink
            #     self.bot.AddAsset(col_pink)
            
            self.system.Add(self.bot)
            self.bots.append(self.bot)
            
            #-----------------------------------
            #### mem==1
            #----------------------------------
            
            if self.mem==1: # Adds inter-particles for anti-escape to occur. AKA skin here!
                
                b_ang=2*np.pi/self.num_bots              # angle between centers of bots
                o_ang=np.arctan(self.diameter/self.R)   # angle offset for radius of bot
                p_ang=np.arctan(self.skind/self.R)           # angle offset for radius of skin particle
                
                skin_id_1 = self.num_bots + self.num_interior + 1 # The first number ID for skin praticles
                
                # Between this bot and last bot
                if i>=1 and i<self.num_bots:
                    for j in range(1, self.ratioM+1,1):
                        # Initial postion of each particle
                        theta=i*b_ang + j*(b_ang-o_ang-p_ang)/(self.ratioM) + p_ang
                        x=self.R*np.cos(theta) - self.X_targ
                        y=.52*self.height
                        z=self.R*np.sin(theta) - self.Z_targ
                        
                        # Store Positions
                        self.skin_posX.append(x)
                        self.skin_posZ.append(z)
                        
                        # Create particles    
                        skinm = chrono.ChBody()
                        skinm = chrono.ChBodyEasyCylinder(self.skind/2, .75*self.height, self.skinrho)
                        skinm.SetPos(chrono.ChVectorD(x,y,z))
                        skinm.SetMaterialSurface(self.material)
                        skinm.SetNoGyroTorque(True)
                        
                        # Set IDs for Collision Detection
                        skinm.SetId(int(skin_id_1)) 
                        skin_id_1 += 1 
                        
                        # rotate them
                        rotation1 = chrono.ChQuaternionD()
                        rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
                        skinm.SetRot(rotation1)
                        
                        # collision model
                        skinm.GetCollisionModel().ClearModel()
                        skinm.GetCollisionModel().AddCylinder(self.skind/2,self.skind/2,(.75*self.height/2)) # hemi sizes
                        skinm.GetCollisionModel().BuildModel()
                        skinm.SetCollide(True)
                        
                        # Set a mate to the floor
                        pt=chrono.ChLinkMatePlane()
                        pt.Initialize(self.body_floor,skinm,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
                        self.system.AddLink(pt)
                        
                        # Attach springs   
                        if j>1:
                            ground=chrono.ChLinkSpring()
                            p1=0; p2=self.skind/2
                            p3=0; p4=-self.skind/2
                            h=self.height/4
                    
                            ground.Initialize(self.skinM[-1], skinm, True, chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4), False)
                            ground.Set_SpringK(self.k)
                            ground.Set_SpringR(self.b)
                            ground.Set_SpringRestLength(self.rl)
                            ground.AddAsset(col_b)
                            ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                            self.Springs.append(ground)
                            self.system.AddLink(ground)
                            
                            ground1=chrono.ChLinkSpring()
                            ground1.Initialize(self.skinM[-1], skinm, True, chrono.ChVectorD(p1,-h,p2), chrono.ChVectorD(p3,-h,p4), False)
                            ground1.Set_SpringK(self.k)
                            ground1.Set_SpringR(self.b)
                            ground.Set_SpringRestLength(self.rl)
                            ground1.AddAsset(col_b)
                            ground1.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                            self.Springs.append(ground1)
                            self.system.AddLink(ground1)

                        #Link to cylinder
                        if j==1:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(skinm,self.bots[i])
                            skinm.SetCollide(False)
                            self.system.AddLink(glue)
                            # Link last particle with this bot
                            if i>=2:
                                glue=chrono.ChLinkMateFix()
                                glue.Initialize(self.skinM[-1],self.bots[-1])
                                self.skinM[-1].SetCollide(False)
                                self.system.AddLink(glue)                                
                            
                        if j==self.ratioM:
                            skinm.AddAsset(col_b)
                            
                        self.system.Add(skinm)
                        self.skinM.append(skinm)
                    
                # Between this bot and first bot
                if i==self.num_bots-1:
                    for j in range(1,self.ratioM+1):
                        # Initial postion of each skin particle
                        theta=(i+1)*b_ang + j*(b_ang-o_ang-p_ang)/(self.ratioM) + p_ang
                        x=self.R*np.cos(theta) - self.X_targ
                        y=.52*self.height
                        z=self.R*np.sin(theta) - self.Z_targ
                        
                        # Store positions
                        self.skin_posX.append(x)
                        self.skin_posZ.append(z)
                        
                        # Create particles    
                        skinm = chrono.ChBody()
                        skinm = chrono.ChBodyEasyCylinder(self.skind/2, .75*self.height, self.skinrho)
                        skinm.SetPos(chrono.ChVectorD(x,y,z))
                        skinm.SetMaterialSurface(self.material)
                        skinm.SetNoGyroTorque(True)
                        
                        # Set a mate to the floor
                        pt=chrono.ChLinkMatePlane()
                        pt.Initialize(self.body_floor,skinm,False,
                                      chrono.ChVectorD(0,0,0),
                                      chrono.ChVectorD(0,0,0),
                                      chrono.ChVectorD(0,1, 0),
                                      chrono.ChVectorD(0,-1, 0))
                        self.system.AddLink(pt)
                        
                        skinm.SetId(int(skin_id_1)) # Set the skin ID for collision detection
                        skin_id_1 += 1
                        
                        # rotate them
                        rotation1 = chrono.ChQuaternionD()
                        rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
                        skinm.SetRot(rotation1)
                        
                        # collision model
                        skinm.GetCollisionModel().ClearModel()
                        skinm.GetCollisionModel().AddCylinder(self.skind/2,self.skind/2,(.75*self.height/2)) # hemi sizes
                        skinm.GetCollisionModel().BuildModel()
                        skinm.SetCollide(True)
                        

                        # Attach springs    
                        if j>1:
                            col_y = self.chrono_color('yellow')
                            ground=chrono.ChLinkSpring()
                            p1=0; p2=self.skind/4
                            p3=0; p4=-self.skind/4
                            h=self.height/4
                    
                            ground.Initialize(self.skinM[-1], skinm, True,chrono.ChVectorD(p1,h,p2), chrono.ChVectorD(p3,h,p4),False)
                            ground.Set_SpringK(self.k)
                            ground.Set_SpringR(self.b)
                            ground.Set_SpringRestLength(self.rl)
                            ground.AddAsset(col_y)
                            ground.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                            self.Springs.append(ground)
                            self.system.AddLink(ground)
                            
                            ground1=chrono.ChLinkSpring()
                            ground1.Initialize(self.skinM[-1], skinm, True, chrono.ChVectorD(p1,-h,p2), chrono.ChVectorD(p3,-h,p4),False)
                            ground1.Set_SpringK(self.k)
                            ground1.Set_SpringR(self.b)
                            ground.Set_SpringRestLength(self.rl)
                            ground1.AddAsset(col_y)
                            ground1.AddAsset(chrono.ChPointPointSpring(.01,80,15))
                            self.Springs.append(ground1)
                            self.system.AddLink(ground1)  
                            
                        #Link to cylinder
                        if j==1:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(skinm,self.bots[0])
                            skinm.SetCollide(False)
                            self.system.AddLink(glue)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(self.skinM[-1],self.bots[0])
                            self.system.AddLink(glue)
                         
                        if j==self.ratioM:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            skinm.SetCollide(False)
                            glue.Initialize(skinm,self.bots[1])
                            self.system.AddLink(glue)
                            
                        self.system.Add(skinm)
                        self.skinM.append(skinm)
                        
                        
            #--------------------------
            #### mem==2
            #--------------------------
            if self.mem==2: # Adds inter-particles for anti-escape to occur. AKA skin here!
                
                b_ang=2*np.pi/self.num_bots              # angle between centers of bots
                o_ang=np.arctan(self.diameter/self.R)   # angle offset for radius of bot
                p_ang=np.arctan(self.skind/self.R)           # angle offset for radius of skin particle
                
                skin_id_1 = self.num_bots + self.num_interior + 1 # The first number ID for skin praticles
                
                # Between this bot and last bot
                if i>=1 and i<self.num_bots:
                    for j in range(1, self.ratioM+1,1):
                        # Initial postion of each particle
                        theta=i*b_ang + j*(b_ang-o_ang-p_ang)/(self.ratioM) + p_ang
                        x=self.R*np.cos(theta) - self.X_targ
                        y=.52*self.height
                        z=self.R*np.sin(theta) - self.Z_targ
                        

                        # Create particles    
                        skinm = chrono.ChBody()
                        skinm = chrono.ChBodyEasyCylinder(self.skind/2, .75*self.height, self.skinrho, True, True)
                        skinm.SetPos(chrono.ChVectorD(x,y,z))
                        skinm.SetMaterialSurface(self.material)
                        skinm.SetNoGyroTorque(True)
                        
                        # Set IDs for Collision Detection
                        skinm.SetId(int(skin_id_1)) 
                        skin_id_1 += 1 
                        
                        # rotate them
                        rotation1 = chrono.ChQuaternionD()
                        rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
                        skinm.SetRot(rotation1)
                        
                        # Set a mate to the floor
                        pt=chrono.ChLinkMatePlane()
                        pt.Initialize(self.body_floor,skinm,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
                        self.system.AddLink(pt)
                        
                        # Attach Rotation Lock and Distance Constraint
                        if j>1:

                            #Forces the two bodies to be a certain distance from eachother
                            dis_lock = chrono.ChLinkDistance()
                            dis_lock.Initialize(self.skinM[-1],
                                                skinm,
                                                True,
                                                chrono.ChVectorD(0,0,0),
                                                chrono.ChVectorD(0,0,0))
                            self.system.AddLink(dis_lock)
    
                        #Link to cylinder
                        if j==1:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(skinm,self.bots[i])
                            self.system.AddLink(glue)
                            
                            # Link last particle with this bot
                            if i>=2:
                                glue=chrono.ChLinkMateFix()
                                glue.Initialize(self.skinM[-1], self.bots[-1])
                                self.system.AddLink(glue)                                
                            
                        if j==self.ratioM:
                            col_pink=self.chrono_color('pink')
                            skinm.AddAsset(col_pink)
                            
                        self.system.Add(skinm)
                        self.skinM.append(skinm)
                    
                # Between this bot and first bot
                if i==self.num_bots-1:
                    for j in range(1,self.ratioM+1):
                        # Initial postion of each skin particle
                        theta=(i+1)*b_ang + j*(b_ang-o_ang-p_ang)/(self.ratioM) + p_ang
                        x=self.R*np.cos(theta) - self.X_targ
                        y=.52*self.height
                        z=self.R*np.sin(theta) - self.Z_targ
                        
                        # Store positions
                        self.skin_posX.append(x)
                        self.skin_posZ.append(z)
                        
                        # Create particles    
                        skinm = chrono.ChBody()
                        skinm = chrono.ChBodyEasyCylinder(self.skind/2, .75*self.height, self.skinrho, True, True)
                        skinm.SetPos(chrono.ChVectorD(x,y,z))
                        skinm.SetMaterialSurface(self.material)
                        skinm.SetNoGyroTorque(True)
                        
                        # Set a mate to the floor
                        pt=chrono.ChLinkMatePlane()
                        pt.Initialize(self.body_floor,skinm,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
                        self.system.AddLink(pt)
                        
                        skinm.SetId(int(skin_id_1)) # Set the skin ID for collision detection
                        skin_id_1 += 1
                        
                        # rotate them
                        rotation1 = chrono.ChQuaternionD()
                        rotation1.Q_from_AngAxis(-theta, chrono.ChVectorD(0, 1, 0));  
                        skinm.SetRot(rotation1)
                        
                        # Distance Constraint 
                        if j>1:
                            dis_lock = chrono.ChLinkDistance()
                            dis_lock.Initialize(self.skinM[-1],
                                                skinm,
                                                True,
                                                chrono.ChVectorD(0,0,0),
                                                chrono.ChVectorD(0,0,0))
                            self.system.AddLink(dis_lock)
                            
                        #Link to cylinder
                        if j==1:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(skinm,self.bots[0])
                            self.system.AddLink(glue)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(self.skinM[-1],self.bots[0])
                            self.system.AddLink(glue)
                         
                        if j==self.ratioM:
                            skinm.AddAsset(col_b)
                            glue=chrono.ChLinkMateFix()
                            glue.Initialize(skinm,self.bots[1])
                            self.system.AddLink(glue)
                            
                        self.system.Add(skinm)
                        self.skinM.append(skinm)
                        
        #---------------------
        #### Add Interior
        #---------------------

        if self.mem==0: # If there is no membrane:
            n = self.num_bots + 1 # This is the first index ID for particles
            i=0
            for radius in self.in_rings_radius:
                if radius != 0: # Placing interiors wherever there is a ring
                    for j in range(self.gran_per_ring[i]): 
                        x = radius*np.cos(j*2*np.pi/self.gran_per_ring[i]) - self.X_targ
                        y = .5*self.height
                        z = radius*np.sin(j*2*np.pi/self.gran_per_ring[i]) - self.Z_targ
                        
                        self.gran = chrono.ChBody()
                        self.gran = chrono.ChBodyEasyCylinder(self.diameter/2, self.height, self.rho_in,True,True)
                        self.gran.SetPos(chrono.ChVectorD(x,y,z))
                        self.gran.SetMaterialSurface(self.material)
                        self.gran.SetId(n)
                        
                        self.gran.SetCollide(True)
                        self.gran.SetBodyFixed(False)
                        
                        # Set speed limit
                        self.bot.SetMaxSpeed(self.speed_limit)
                        self.bot.SetLimitSpeed(True)
                        
                        col_r = self.chrono_color('red')
                        self.gran.AddAsset(col_r)
                        
                        pt=chrono.ChLinkMatePlane()
                        pt.Initialize(self.body_floor, self.gran, False, chrono.ChVectorD(0,0,0), chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1,0),chrono.ChVectorD(0,-1,0))
                        self.system.AddLink(pt)
                        self.system.Add(self.gran)
                        self.interior.append(self.gran)
                        
                        # Interior(x,y,z,n,self.diameter,self.height,self.rho_in,radius,self.material,self.interior,self.system,self.body_floor)
                        n += 1 # Move to the next ID for interior particles
                    i += 1 # Move to the next index in self.gran_per_ring
                    
        if self.mem in range(1,5): # If there IS a membrane, create interiors this way.
            n = self.num_bots + 1 # This is the first index ID for particles
            count=0
            for i in range(self.n.size):
                for j in range(self.n[i]):
                    count=count+1
        
                    R2=self.diameter*self.n[i]/(np.pi)
                    x=R2*np.cos(j*2*np.pi/self.n[i]) - self.X_targ
                    y=.5*self.height
                    z=R2*np.sin(j*2*np.pi/self.n[i]) - self.Z_targ
                    # create body
                    gran = chrono.ChBody()
                    #gran = chrono.ChBodyEasySphere(self.diameter2,self.rowp,True,True)
                    gran = chrono.ChBodyEasyCylinder(self.diameter, self.height,self.rho_in,True,True)
                    gran.SetPos(chrono.ChVectorD(x,y,z))
                    gran.SetMaterialSurface(self.material)
                    
                    gran.SetId(n) # Set the ID for interior particles
                    n += 1
                    
                    gran.SetCollide(True)
                    gran.SetBodyFixed(False)
                    # add color
                    col_r = self.chrono_color('red')
                    gran.AddAsset(col_r)
                    # mate to floor
                    pt=chrono.ChLinkMatePlane()
                    pt.Initialize(self.body_floor,gran,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
                    self.system.AddLink(pt)
                    
                    # add to system
                    self.system.Add(gran)
                    self.interior.append(gran)
                    
        #-------------------------------------------
        #### Add a pink cylinder at the target
        #-------------------------------------------
        target_color=self.chrono_color('pink')
        
        self.target = chrono.ChBodyEasyCylinder(self.diameter/2, self.height*2, self.rho_bot)
        self.target.SetPos(chrono.ChVectorD(0, self.height, 0))
        self.target.SetCollide(False)
        self.target.SetBodyFixed(True)
        self.target.AddAsset(target_color)
        
        self.system.Add(self.target)
        
        #---------------------------------
        #### Create an Obstacle Field
        #--------------------------------
        
        # Field Parameters:        
        self.field_width = 16*self.R
        self.field_height = 10*self.R
        
        num_obstacles=0
        # min_obs_dis = 6*self.R #Used this for no deformation of system required
        min_obs_dis = 6*self.R
        if self.mem==0:
            JAMoEBA_size=int(self.num_bots+self.num_interior) 
        if self.mem in range(1,5):
            JAMoEBA_size=int(self.num_bots+self.num_interior+self.num_skin)
        
        # poisson_samples = Poisson_Sampling(min_obs_dis, self.field_width, self.field_height)
        # obstacle_coordinates = poisson_samples.get_samples()
        # self.obstacle_field(JAMoEBA_size, obstacle_coordinates, self.material, self.field_width, self.field_height, shape='box')
        
        # num_obstacles = len(obstacle_coordinates)
        
        #---------------------------
        #### Objects to Manipulate
        # Creating a ball to push
        #---------------------------
            #### Ball
        # ball_radius = self.R/2
        # ball_density = self.rho_bot
        # ball_color=self.chrono_color('green')
        
        # if self.mem==0:
        #     JAMoEBA_size=int(self.num_bots+self.num_interior) 
        # if self.mem in range(1,5):
        #     JAMoEBA_size=int(self.num_bots+self.num_interior+self.num_skin)
        
        # self.ball = chrono.ChBodyEasyCylinder(ball_radius, self.height, self.rho_bot, True, True)
        # self.ball.SetPos(chrono.ChVectorD(-self.X_targ+2*self.R+0.5, self.height/2, -self.Z_targ))
        # self.ball.SetMaterialSurface(self.material)
        # self.ball.SetCollide(True)
        # self.ball.SetBodyFixed(False)
        # self.ball_fall=chrono.ChLinkMatePlane()
        # self.ball_fall.Initialize(self.body_floor,self.ball,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
        # self.system.AddLink(self.ball_fall)
        # self.ball.SetId(int(JAMoEBA_size+1))
        # self.obstacles.append(int(JAMoEBA_size+1))
        # self.ball.AddAsset(ball_color)
        # self.system.Add(self.ball)
        
        #----------------------------
        #### Two Ball Gap to Squeeze Through
        #----------------------------
        # gap_size=self.R*(10/16)
        # gap_obstacle_radius=0.5*self.field_height
        # num_obstacles=2
        
        # #Position will change, so let's randomize them
        # gap_X_pos = (8*self.R*np.random.rand())-16*self.R # X \in [-16R-8R]
        # gap_Z_pos = (6*self.R*np.random.rand())-3*self.R  # Z \in [-3R,3R]

        # self.top_gap=chrono.ChBodyEasyCylinder(gap_obstacle_radius, self.height, self.rho_bot,True,True)
        # self.top_gap.SetPos(chrono.ChVectorD(gap_X_pos, self.height/2, gap_Z_pos + (0.5*gap_size + gap_obstacle_radius)))
        # self.top_gap.SetMaterialSurface(self.material)
        # self.top_gap.SetCollide(True)
        # self.top_gap.SetBodyFixed(True)
        # self.top_gap.AddAsset(self.chrono_color('green'))
        # self.top_gap.SetId(int(JAMoEBA_size+1))
        # self.obstacle_ids.append(int(JAMoEBA_size+1))
        # self.system.Add(self.top_gap)
        
        # self.bottom_gap=chrono.ChBodyEasyCylinder(gap_obstacle_radius, self.height, self.rho_bot,True,True)
        # self.bottom_gap.SetPos(chrono.ChVectorD(gap_X_pos, self.height/2, gap_Z_pos - (0.5*gap_size + gap_obstacle_radius)))
        # self.bottom_gap.SetMaterialSurface(self.material)
        # self.bottom_gap.SetCollide(True)
        # self.bottom_gap.SetBodyFixed(True)
        # self.bottom_gap.AddAsset(self.chrono_color('green'))
        # self.bottom_gap.SetId(int(JAMoEBA_size+2))
        # self.obstacle_ids.append(int(JAMoEBA_size+2))
        # self.system.Add(self.bottom_gap)
        
        #------------------------------------
        #### Two Wall gap to squeeze through
        #------------------------------------
        
        # gap_size=self.R*(10/16)
        # gap_obstacle_height=self.field_height
        # num_obstacles=2
        
        # #Position will change, so let's randomize them
        # gap_X_pos = (8*self.R*np.random.rand())-16*self.R # X \in [-16R-8R]
        # gap_Z_pos = (6*self.R*np.random.rand())-3*self.R  # Z \in [-3R,3R]

        # self.top_gap=chrono.ChBodyEasyBox(gap_obstacle_height, self.height, gap_obstacle_height, self.rho_bot,True,True)
        # self.top_gap.SetPos(chrono.ChVectorD(gap_X_pos, self.height/2, gap_Z_pos + (0.5*gap_size + gap_obstacle_height)))
        # self.top_gap.SetMaterialSurface(self.material)
        # self.top_gap.SetCollide(True)
        # self.top_gap.SetBodyFixed(True)
        # self.top_gap.AddAsset(self.chrono_color('green'))
        # self.top_gap.SetId(int(JAMoEBA_size+1))
        # self.obstacle_ids.append(int(JAMoEBA_size+1))
        # self.system.Add(self.top_gap)
        
        # self.bottom_gap=chrono.ChBodyEasyBox(gap_obstacle_height, self.height, gap_obstacle_height, self.rho_bot,True,True)
        # self.bottom_gap.SetPos(chrono.ChVectorD(gap_X_pos, self.height/2, gap_obstacle_height + gap_size - gap_Z_pos - (0.5*gap_size + gap_obstacle_height)))
        # self.bottom_gap.SetMaterialSurface(self.material)
        # self.bottom_gap.SetCollide(True)
        # self.bottom_gap.SetBodyFixed(True)
        # self.bottom_gap.AddAsset(self.chrono_color('green'))
        # self.bottom_gap.SetId(int(JAMoEBA_size+2))
        # self.obstacle_ids.append(int(JAMoEBA_size+2))
        # self.system.Add(self.bottom_gap)
        
        #-------------------
        #### Bounding Walls
        #-------------------
        wall_length = self.X_targ + 2
        wall_width=.1
        wall_color=self.chrono_color('green')
        
        wall_x = -wall_length/2
        wall_z = wall_width/2 + self.field_height/2 + self.obstacle_radius
        
        ID = int(JAMoEBA_size+num_obstacles)+1
        self.obstacle_ids.append(ID)
        self.obstacle_ids.append(ID+1)
        
        self.system.Add(Wall(self.material, wall_length, wall_width, self.height, wall_x, wall_z, wall_color, ID)) # Top Wall
        self.system.Add(Wall(self.material, wall_length, wall_width, self.height, wall_x, -wall_z,wall_color, ID+1)) # Bottom Wall
        
        # Back Wall
        wall_x = -(self.X_targ + wall_width/2 + self.R + self.diameter + 0.01) # The final term is a buffer
        self.obstacle_ids.append(ID+2)
        self.system.Add(Wall(self.material, wall_width, self.field_height+2*self.R, self.height, wall_x, 0, wall_color, ID+2))

        #-----------------------------------
        #     Get an observation to report
        #### Initialize steps    
        #-----------------------------------
        self.previous_distance = self.start_dis
        self.numsteps = 0
        self.step(np.zeros(self.action_size))
  
        ac=np.zeros(self.num_bots*2)
        return self.get_ob(ac)

    def step(self, ac):
        """
        Step Function - Take an action!
        In this environment, the action will be distributed such that for 30 bots (60 actions typically) there will only be 20 actions.
        Each action will be a force in a direction for *3 congruent bots*.
        """       
        self.numsteps += 1    # This is specific to ONLY the episode currently being run.
        # self.global_time += 1 # This is for a single training run (i.e. a single experiment)
        actions=np.zeros(2*self.num_bots)
        index = 0
        indexZ=0
        for i, action in enumerate(ac):
            for j in range(3):
                if i<self.half_ac_size:
                    self.forcesX[index].SetMforce(self.gain*action)
                else:
                    self.forcesZ[indexZ].SetMforce(self.gain*action)
                    indexZ+=1
                actions[index]=action
                index+=1
                
        # self.apply_river()
        
        if self.mem !=2:
            self.stiffSpring() # Stiffen the springs
        
        power_used = self.power_consumption()
        
        self.system.DoStepDynamics(self.timestep)
        self.time += self.timestep
        obs = self.get_ob(actions)
        rew = self.calc_rew(actions)
        

        
        if self.data_collect:
            self.data_collection(actions, rew, obs)
        
        return obs, rew, self.isdone, self.info
      
    def get_ob(self, ac):
        """
        Observation Function - What is our current state
        Note: This is the OLD observation function. The new one above incorporates moving averages
        """ 
        bot_pos=[]
        bot_vel=[]
        bot_external_forces=np.zeros(2*self.num_bots) # Initialize the forces as all 0
        in_pos=[]
        in_vel=[]
        
        # For power consumption processing
        self.X_Positions = np.roll(self.X_Positions,1,0)
        self.Z_Positiions = np.roll(self.Z_Positions,1,0)
        self.X_Actions = np.roll(self.X_Actions,1,0)
        self.Z_Actions = np.roll(self.Z_Actions,1,0)
        
        # Observing the positions, velocities, and forces:
        for i in range(self.num_bots):
            
            # Note the terms normalizing the position and forces
            
            # X-Direction Variables
            bot_x = self.bots[i].GetPos().x
            bot_pos.append(bot_x/(self.X_targ+3*self.R)) # Position is relative to the target position.
            bot_vel.append(self.bots[i].GetPos_dt().x/self.speed_limit) #Speed is normalized
            self.X_Positions[0][i] = bot_x
            self.X_Actions[0][i] = ac[2*i]*self.gain 
            
            # Z-Direction Variables
            bot_z = self.bots[i].GetPos().z
            bot_pos.append(bot_z/(self.field_height+3*self.R)) # Position is relative to the target position.
            bot_vel.append(self.bots[i].GetPos_dt().z/self.speed_limit) #Speed is normalized
            self.Z_Positions[0][i] = bot_z
            self.Z_Actions[0][i] = ac[2*i+1]*self.gain   
        
        # Observing external forces on the bots:
        self.my_rep.ResetList()
        self.system.GetContactContainer().ReportAllContacts(self.my_rep)
        contact_list = self.my_rep.GetList()
        
        colliding_objects = contact_list[6]
        x_collision_forces = contact_list[3]
        z_collision_forces = contact_list[5]
        
        collision_number = 0 # Use this to iterate through the collisions
        for collision in colliding_objects:
            # Each collision gives a collision pair.
            # For example, [1,2] means objects with ID 1 and 2 are colliding.
            for bot_id in range(1,self.num_bots+1):
                if bot_id in collision:  # Check if the collision is with a bot
                    # Need to make sure the collision is with an external object                    
                    for obstacle in self.obstacle_ids:
                        if obstacle in collision:
                            # NOW we can add the forces!
                            bot_external_forces[bot_id*2-2] += x_collision_forces[collision_number]
                            bot_external_forces[bot_id*2-1] += z_collision_forces[collision_number]
            collision_number += 1
            
        self.temp_contact=bot_external_forces
        bot_forces_norm = np.linalg.norm(bot_external_forces)
        if bot_forces_norm != 0: bot_external_forces = bot_external_forces/bot_forces_norm
            
        #Put it all together now!
        run_time=np.array([self.numsteps*self.timestep/self.kill_time]) # Relative time to the end of the simulation
        obs = np.concatenate((bot_pos, bot_vel, ac, bot_external_forces, run_time)) # Not including interiors 
        return obs
    

    def calc_rew(self, ac):
        """
        Reward Function - How good is this?
        """
        
        # progress = self.calc_progress()
        # rew = progress*750 - (current_distance/self.start_dis)
        x,z = self.center()
        current_distance=np.linalg.norm([x,z])
        # dis_penalty= self.distance_limit()
        
        
        # Reward for moving forwards
        if current_distance < self.previous_distance:
            rew = ((self.start_dis/self.start_dis)-(current_distance/self.start_dis))*self.Rew_Constant
        else:
            rew=0
        
        # Time penalty on Reward
        time_penalty = (self.numsteps/self.max_num_timestep)*self.Rew_Constant*0.5
        # rew -= time_penalty
        
        # Reward for contact with obstacles
        contact_penalty = np.linalg.norm(self.temp_contact)*.05
        # rew -= contact_penalty
        
        rew = self.is_done(rew) #- dis_penalty*0.01 # Changes the reward if we reach a terminal state. If not, no worries!

        self.previous_distance = current_distance
        return rew

    def calc_progress(self):           
        """
        Progress function - How close are we to the target
        """
        # Find the center of JAMoEBA
        x_center, z_center = self.center()
        
        d = np.linalg.norm([x_center, z_center])
        progress = -(d - self.d_old)        
        self.d_old = d
        
        return progress
    

    def power_consumption(self):            
        """
        Power Consumption - How much energy was used in this step?
        Multiples F*dx
        """
        
        past_x = np.mean(self.X_Positions[:self.midway][:],axis=0)
        current_x = np.mean(self.X_Positions[self.midway:][:],axis=0)
        
        past_z = np.mean(self.Z_Positions[:self.midway][:],axis=0)
        current_z = np.mean(self.Z_Positions[self.midway:][:],axis=0)
        
        past_X_actions = np.mean(self.X_Actions[self.midway:][:],axis=0)
        current_X_actions = np.mean(self.X_Actions[:self.midway][:],axis=0)
        
        past_Z_actions = np.mean(self.Z_Actions[self.midway:][:],axis=0)
        current_Z_actions = np.mean(self.Z_Actions[:self.midway][:],axis=0)
    
        dx = current_x - past_x
        dz = current_z - past_z
        
        delta_force_x = current_X_actions - past_X_actions
        delta_force_z = current_Z_actions - past_Z_actions
        
        Px = delta_force_x*dx
        Pz = delta_force_z*dz
        
        power_used = sum(abs(Px)) + sum(abs(Pz))
        self.tot_power += power_used
        
        return power_used
    
    #TODO: Remove this if springs will not be used moving forward.
    def distance_limit(self):
        
        dis_penalty = 0
        if self.mem==0:
            dis_threshold = 0.1
            
        if self.mem in range(1,5):
            dis_threshold = 0.25
        
        for i in range(self.num_bots):
            if i >= 1:
                dis_x = self.bots[i].GetPos().x - self.bots[i-1].GetPos().x
                dis_z = self.bots[i].GetPos().z - self.bots[i-1].GetPos().z
                distance = np.linalg.norm([dis_x, dis_z])
                
                if distance > dis_threshold:
                    dis_penalty += distance
            
            # Last bot to first bot
            if i==(self.num_bots-1):
                dis_x = self.bots[i].GetPos().x - self.bots[0].GetPos().x
                dis_z = self.bots[i].GetPos().z - self.bots[0].GetPos().z
                distance = np.linalg.norm([dis_x, dis_z])
                
                if distance > dis_threshold:
                    dis_penalty += distance
                
        return dis_penalty
    
    

    def center(self):
        """
        Calculates the geometric center of the system
        """
        x_center = 0
        z_center = 0
        for i in range(self.num_bots):
            x_center += self.bots[i].GetPos().x
            z_center += self.bots[i].GetPos().z
        x_center /= self.num_bots
        z_center /= self.num_bots
        
        return(x_center, z_center)
        

    #TODO: Remove this later. If springs will not be used moving forward from now, then this is not good.
    def stiffSpring(self):
        """
        If the springs are stretched too much, then they are stiffened
        """ 
        for spring in self.Springs:
            spring_length = spring.Get_SpringLength()
        
        if spring_length < self.spring_max:
            spring.Set_SpringK(self.k)
    
        elif spring_length >= self.spring_max:
            spring.Set_SpringK(self.k_stiff)

    def apply_river(self):
        """
        Applies a river force dependent on the location of the bot.
        """
        x_com, z_com = self.center()
        length=self.system_length()
        normalizing_agent=length/2 #Empirical
        
        ind=1
        for bot in self.bots:
            bot_x = bot.GetPos().x
            
            # Check if the bot is in the river
            if -20*self.R < bot_x < -4*self.R:
                
                # Check the quadrant of the bot
                quad = quadrant(bot_x - x_com, bot.GetPos().z - z_com)
                if quad==1 or quad==2:
                    # Apply the river force to the bot
                    current_z = self.forces[2*ind-1].GetMforce()
                    
                    self.forces[2*ind-1].SetMforce(current_z + self.river_force*normalizing_agent)
                    
    def system_length(self):
        """
        Returns the x-length of the system
        """
        exes = np.zeros(self.num_bots)
        for i in range(self.num_bots):
            exes[i] = self.bots[i].GetPos().x
            
        length=max(exes) - min(exes)
        return(length)
                    
    def is_done(self, rew):
        """
        Done function - Kills the episode if it takes too long
        """  
        x_center, z_center = self.center()
        
        if (self.numsteps*self.timestep>self.kill_time or abs(x_center)>=self.X_targ+3*self.R or abs(z_center)>=self.field_height+3*self.R): # Take too long or left the playing field.
            self.isdone = True
            rew -= 0 # Penalizing going off track or takes too long to get there.
            return rew
            
        elif (abs(x_center) <= 0.5 and abs(z_center) <= 0.5): # Arrived at target
            self.isdone = True
            rew += 10 # Add an extra 10 points for reaching the target
            return rew
        
        elif x_center > 1.0:
            # If the system overshoots the x_target
            self.isdone = True
            rew -=0
            return rew
        
        else:
            return rew # Does not change the reward
        
    def obstacle_field(self,JAMoEBA_size, obstacle_coordinates, material, field_width, field_height, shape='cylinder'):
        """
        JAMoEBA_size: Current number of physical objects that make up JAMoEBA
        obstacle_coord: List of coordinates for obstacles to be placed in gathered by Poisson Sampling
        Material: Material for the obstacles
        """
        
        # Now let's make the obstacles:
        green_color = self.chrono_color('green')
        
        e=0
        for coord in obstacle_coordinates:
            if shape=='cylinder': self.obstacles.append(chrono.ChBodyEasyCylinder(self.obstacle_radius, self.height, self.rho_bot, True, True))
            if shape=='box': self.obstacles.append(chrono.ChBodyEasyBox(1.5*self.obstacle_radius, self.height, 1.5*self.obstacle_radius, self.rho_bot, True, True))
            self.obstacles[e].SetPos(chrono.ChVectorD(
                obstacle_coordinates[e][0] - field_width - 4*self.R, # X-Pos, slight offset from target
                self.height/2,                                        # Y-Pos
                obstacle_coordinates[e][1] - (field_height/2)))       # Z-Pos
            
            if shape!='cylinder':
                theta=np.random.random()*np.pi
                rotation=chrono.ChQuaternionD()
                rotation.Q_from_AngAxis(theta,chrono.ChVectorD(0,1,0))
                self.obstacles[e].SetRot(rotation)
            
            self.obstacles[e].SetMaterialSurface(material)

            self.obstacles[e].SetCollide(True)
            self.obstacles[e].SetBodyFixed(True)
            
            self.obstacles[e].AddAsset(green_color)
            
            if self.mem==0:
                self.obstacles[e].SetId(int(JAMoEBA_size + e + 1))
                self.obstacle_ids.append(int(JAMoEBA_size + e + 1))
            
            if self.mem in range(1,5):
                self.obstacles[e].SetId(int(JAMoEBA_size + e + 1))
                self.obstacle_ids.append(int(JAMoEBA_size + e + 1))
            self.system.Add(self.obstacles[e])            
            e+=1
            
    def chrono_color(self, color=None):
        """
        Options include:
            green
            blue
            red
            pink
            grey
            purple
            yellow
        """
        
        if color=='pink':
            Chcolor=chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(1,0.7529,0.7961))
            
        if color=='green':
            Chcolor=chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(0,1,0))
            
        if color=='red':
            Chcolor=chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(1,0,0))
            
        if color=='grey':
            Chcolor = chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(.75,.75,.75))
            
        if color=='purple':
            Chcolor = chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(0.502,0,0.502))
        
        if color=='yellow':
            Chcolor=chrono.ChColorAsset()
            Chcolor.SetColor(chrono.ChColor(1,1,0))
        
        return Chcolor
            
    def render(self):
        if not self.render_setup:
            #### POV_Ray Setup
            if self.POV_Ray: 
                #chrono.SetChronoDataPath("C:/Chrono/Chrono_dependencies/Chrono_develop/data/") #Laptop
                # chrono.SetChronoDataPath("C:/chrono/chrono_dependencies/data/")					#Desktop
                chrono.SetChronoDataPath('C:/chrono/dependencies/chrono-master/data/')          #Big Gucci
                self.script_dir = os.path.dirname('povvideofiles' + self.experiment_name + '/')
                self.pov_exporter = postprocess.ChPovRay(self.system)
                
                # Sets some file names for in-out processes
                self.pov_exporter.SetTemplateFile(chrono.GetChronoDataPath() + "_template_POV.pov") # This line may be redundant according to Chrono's website
                self.pov_exporter.SetOutputScriptFile("rendering_" + self.experiment_name + ".pov")
                self.pov_exporter.SetOutputDataFilebase("my_state")
                self.pov_exporter.SetPictureFilebase("picture")

                # Creates folders
                self.POV_output = self.experiment_name+"_output"
                self.POV_anim = self.experiment_name+"_anim"
                if not os.path.exists(self.POV_output):
                    os.mkdir(self.POV_output)
                if not os.path.exists(self.POV_anim):
                    os.mkdir(self.POV_anim)
                    
                x,z = self.center()
                self.pov_exporter.SetOutputDataFilebase(self.POV_output + "/my_state")
                self.pov_exporter.SetPictureFilebase(self.POV_anim + "/picture")
                self.pov_exporter.SetCamera(chrono.ChVectorD(-self.X_targ/2, self.R*5, -self.Z_targ/2), # Camera Location
                                            chrono.ChVectorD(-self.X_targ/2, 0, -self.Z_targ/2),# Camera Point
                                            90) #Camera Angle
                
                self.pov_exporter.AddAll()
                self.pov_exporter.ExportScript()
                self.count = 0
                self.render_setup = True
                
            #### Irrlicht      
            else: 
                self.myapplication = chronoirr.ChIrrApp(self.system, self.experiment_name, chronoirr.dimension2du(1024,768))
                self.myapplication.AddShadowAll()
                self.myapplication.SetTimestep(self.timestep)
                self.myapplication.AddTypicalSky(chrono.GetChronoDataPath() + '/skybox/')
                
                cam_x, cam_z = self.center()
                
                self.myapplication.AddTypicalCamera(chronoirr.vector3df(cam_x, self.R*8, cam_z), # Camera Placement
                                                    chronoirr.vector3df(cam_x, 0, cam_z)) # Camera Point
                self.myapplication.AddLightWithShadow(chronoirr.vector3df(-self.X_targ/2, self.R*14, -self.Z_targ/2),    # point
                                                      chronoirr.vector3df(-self.X_targ/2 ,0, -self.Z_targ/2),      # aimpoint
                                                      20,                 # radius (power)
                                                      1,10,               # near, far
                                                      120)                # angle of FOV
                self.myapplication.AssetBindAll()
                self.myapplication.AssetUpdateAll()
                self.render_setup = True
                
            #### POV_Ray Run
        if self.POV_Ray:
            self.pov_exporter.SetCamera(chrono.ChVectorD(-self.X_targ/2, self.R*14, -self.Z_targ/2), # Camera Location
                                        chrono.ChVectorD(-self.X_targ/2, 0, -self.Z_targ/2), # Camera Point
                                        90) #Camera Angle
            self.pov_exporter.AddAll()
            self.pov_exporter.ExportScript()
            self.count+=1
            if self.count%12 == 0: # Exports every 12th frame
                self.pov_exporter.ExportData()
         
            #### Irrlicht Run    
        else:
            self.myapplication.GetDevice().run()
            self.myapplication.BeginScene()
            self.myapplication.DrawAll()
            self.myapplication.EndScene()
            
            cam_x, cam_z = self.center()
            #### Action camera. 
            #Uncomment if you want the camera to follow the system center
            #self.myapplication.GetSceneManager().getActiveCamera().setPosition(chronoirr.vector3df(cam_x,self.R*8,cam_z))
            #self.myapplication.GetSceneManager().getActiveCamera().setTarget(chronoirr.vector3df(cam_x,0,cam_z))
            
            # TODO: Saves the video, no need for PovRAY
            self.myapplication.SetVideoframeSave(True)
            self.myapplication.SetVideoframeSaveInterval(round(1/(self.timestep*60)))


    def close(self):
        """
        Close Function - Ensures the simulation is closed
        """  
        if self.render_setup:
            if self.POV_Ray:
                None
                
            else:
                self.myapplication.GetDevice().closeDevice()
                print('Destructor called, Device deleted.')
        else:
            print('Destructor called, No device to delete.')
            

    def data_collection(self, ac, rew, obs):
        
        # Create new and empty vectors 
        X_Pos_temp = [self.time]
        Y_Pos_temp = [self.time]
        Z_Pos_temp = [self.time]
        
        X_vel_temp = [self.time]
        Y_vel_temp = [self.time]
        Z_vel_temp = [self.time]
        
        forces_temp = [self.time]
        
        action_temp = [self.time]
        rew_temp = [self.time]
        
        obs_temp = [self.time]
        contact_temp= [self.time]
        
        effort_temp = [self.time]
        
        # Append to the temporary lists
        for i in range(self.num_bots):
            X_Pos_temp.append(self.bots[i].GetPos().x)
            Y_Pos_temp.append(self.bots[i].GetPos().y)
            Z_Pos_temp.append(self.bots[i].GetPos().z)
            
            X_vel_temp.append(self.bots[i].GetPos_dt().x)
            Y_vel_temp.append(self.bots[i].GetPos_dt().y)
            Z_vel_temp.append(self.bots[i].GetPos_dt().z)
            
            forces_temp.append(self.bots[i].Get_Xforce().x)
            forces_temp.append(self.bots[i].Get_Xforce().z)
            
        for i in range(len(ac)):
            action_temp.append(ac[i]*self.gain)
            
        for i in range(len(obs)):
            obs_temp.append(obs[i])
            
        for c in self.temp_contact:
            contact_temp.append(c)
                
        rew_temp.append(rew)
        effort_temp.append(self.tot_power)
        
        # Convert to Numpy Arrays
        X_Pos_temp = np.asarray(X_Pos_temp)
        Y_Pos_temp = np.asarray(Y_Pos_temp)
        Z_Pos_temp = np.asarray(Z_Pos_temp)
        
        X_vel_temp = np.asarray(X_vel_temp)
        Y_vel_temp = np.asarray(Y_vel_temp)
        Z_vel_temp = np.asarray(Z_vel_temp)
        
        forces_temp = np.asarray(forces_temp)
        
        action_temp = np.asarray(action_temp)
        rew_temp = np.asarray(rew_temp)
        
        obs_temp = np.asarray(obs_temp)
        contact_temp = np.asarray(contact_temp)
        effort_temp = np.asarray(effort_temp)
        
        # Now append to the master list
        self.X_data = np.vstack([self.X_data, X_Pos_temp])
        self.X_vel_data = np.vstack([self.X_vel_data, X_vel_temp])
        self.Y_data = np.vstack([self.Y_data, Y_Pos_temp])
        self.Y_vel_data = np.vstack([self.Y_vel_data, Y_vel_temp])
        self.Z_data = np.vstack([self.Z_data, Z_Pos_temp])
        self.Z_vel_data = np.vstack([self.Z_vel_data, Z_vel_temp])
        self.force_data = np.vstack([self.force_data, forces_temp])
        self.ac = np.vstack([self.ac, action_temp])
        self.reward_data = np.vstack([self.reward_data, rew_temp])
        self.obs_data = np.vstack([self.obs_data, obs_temp])
        self.contact_forces=np.vstack([self.contact_forces, contact_temp])
        self.effort = np.vstack([self.effort, effort_temp])
        
            
    def data_export(self):
        # Save the parameters of this experiment:
        txt_file= self.new_folder + 'TESTING_Environment_paramters.txt' 
        
        with open(txt_file, 'w') as f:
            for line in self.environment_parameters:
                f.write("%s\n" % line)
        
        # Delete the temporarily made first row of zeroes
        self.X_data = np.delete(self.X_data, 0, 0)
        self.X_vel_data = np.delete(self.X_vel_data, 0, 0)
        self.Y_data = np.delete(self.Y_data, 0, 0)
        self.Y_vel_data = np.delete(self.Y_vel_data, 0, 0)
        self.Z_data = np.delete(self.Z_data, 0, 0)
        self.Z_vel_data = np.delete(self.Z_vel_data, 0, 0)
        self.force_data = np.delete(self.force_data, 0, 0)
        self.ac = np.delete(self.ac, 0, 0)
        self.reward_data = np.delete(self.reward_data, 0, 0)
        self.obs_data = np.delete(self.obs_data, 0, 0)
        self.contact_forces = np.delete(self.contact_forces,0,0)
        self.effort = np.delete(self.effort,list(range(self.midway*2)),0)
        
        # Save the data on .csv files
        np.savetxt(self.new_folder + 'X_data.csv', self.X_data, delimiter=',')
        np.savetxt(self.new_folder + 'X_vel_data.csv', self.X_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'Y_data.csv', self.Y_data, delimiter=',')
        np.savetxt(self.new_folder + 'Y_vel_data.csv', self.Y_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'Z_data.csv', self.Z_data, delimiter=',')
        np.savetxt(self.new_folder + 'Z_vel_data.csv', self.Z_vel_data, delimiter=',')
        np.savetxt(self.new_folder + 'force_data.csv', self.force_data, delimiter=',')
        np.savetxt(self.new_folder + 'actions.csv', self.ac, delimiter=',')
        np.savetxt(self.new_folder + 'reward.csv', self.reward_data, delimiter=',')
        np.savetxt(self.new_folder + 'observations.csv', self.obs_data, delimiter=',')
        np.savetxt(self.new_folder + 'contacts.csv', self.contact_forces, delimiter=',')
        np.savetxt(self.new_folder + 'Effort.csv', self.effort, delimiter = ',')
        
        if self.plot: # Plot the data now!! 
            self.plot_data()
        
    def parameter_export(self):
        parameter_file=  self.experiment_name + ' TRAINING_environment_parameters.txt'
        
        with open(parameter_file, 'w') as f:
            for line in self.environment_parameters:
                f.write("%s\n" % line)
            f.write('\n')
            f.write("Number of Training Episodes:{}".format(str(self.episode)))

    def plot_data(self):
        # Common Among Position and Velocity Data
        last_col = len(self.X_data[0])-1
        time = self.X_data[:,0]
        xlabel = 'Time [sec]'
        
        # Plot X-Position
        X_COM = []
        for row in self.X_data:
            pos = mean(row[1:last_col])
            X_COM.append(pos)
        plt.figure('X-Pos')
        plt.plot(time,X_COM)
        plt.xlabel(xlabel)
        plt.ylabel('X-Position [m]')
        plt.title('X-Center Position')
        plt.savefig(self.new_folder + 'X-Center Position.jpg')       
        
        # Plot Y-Position
        Y_COM = []
        for row in self.Y_data:
            pos = mean(row[1:last_col])
            Y_COM.append(pos)
        plt.figure('Y-Pos')
        plt.plot(time,Y_COM)
        plt.xlabel(xlabel)
        plt.ylabel('Y-Position [m]')
        plt.title('Y-Center Position')
        plt.savefig(self.new_folder + 'Y-Center Position.jpg')
        
        # Plot Z-Position
        Z_COM = []
        for row in self.Z_data:
            pos = mean(row[1:last_col])
            Z_COM.append(pos)
        plt.figure('Z-Pos')
        plt.plot(time,Z_COM)
        plt.xlabel(xlabel)
        plt.ylabel('Z-Position [m]')
        plt.title('Z-Center Position')
        plt.savefig(self.new_folder + 'Z-Center Position.jpg')
        
        # Plot X-velocity
        plt.figure('X-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.X_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('X Velocity [m/s]')
        plt.title('X-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'X-Velocity.jpg')
        
        # Plot Y-Velocity
        plt.figure('Y-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.Y_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('Y Velocity [m/s]')
        plt.title('Y-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'Y-Velocity.jpg')
        
        # Plot Z-Velocity
        plt.figure('Z-Vel')
        for i in range(self.num_bots):
            plt.plot(time, self.Z_vel_data[:,i+1], label = 'Bot' + str(i+1))
        plt.xlabel(xlabel)
        plt.ylabel('Z Velocity [m/s]')
        plt.title('Z-Velocity')
        plt.legend(loc='lower right')
        plt.savefig(self.new_folder + 'Z-Velocity.jpg')
        
        
        # Plot forces
        last_col_2 = len(self.ac[0])-1
        bot=1
        for i in range(last_col_2):
            if i%2!=0:
                plt.figure('Applied Forces Bot ' + str(bot))
                plt.plot(time, self.ac[:,i], label='X-Force')
                plt.plot(time, self.ac[:,i+1], label='Z-Force')
                plt.xlabel(xlabel)
                plt.ylabel('Force [N]')
                plt.title('Applied Forces on Bot ' + str(bot))
                plt.legend(loc='lower right')
                plt.savefig(self.new_folder + 'Bot ' + str(bot) + ' Applied Forces.jpg')
                bot+=1
                
        # plot contacts
        bot=1
        last_col_3 = len(self.contact_forces[0])-1
        for i in range(last_col_3):
            if i%2!=0:
                plt.figure('External Contacts on Bot '+str(bot))
                plt.plot(time, self.contact_forces[:,i],label='X-Contact')
                plt.plot(time, self.contact_forces[:,i+1],label='Z-Contact')
                plt.xlabel(xlabel)
                plt.ylabel('Contact Force [N]')
                plt.title('External Forces on Bot '+ str(bot))
                plt.legend(loc='lower right')
                plt.savefig(self.new_folder + 'Bot '+str(bot)+' Contact Forces.jpg')
                bot+=1
                
        # Plot reward
        time = self.reward_data[:,0]
        rewards = self.reward_data[:,1]
        plt.figure('Rewards')
        plt.plot(time, rewards)
        plt.xlabel(xlabel)
        plt.ylabel('Reward')
        plt.title('Reward for JAMoEBA')
        plt.savefig(self.new_folder + 'Reward.jpg')
        
        # Plot effort
        effort_across_time = self.effort[:,1]
        plt.figure('Effort [N] Up To Now')
        plt.plot(time[self.midway*2-1:], effort_across_time)
        plt.xlabel(xlabel)
        plt.ylabel('Effort [N]')
        plt.title('Effort [N] Up To Now')
        plt.savefig(self.new_folder + 'Effort.jpg')
            

class MyReportContactCallback(chrono.ReportContactCallback):

    def __init__(self):

        chrono.ReportContactCallback.__init__(self)
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
        self.pointx = []
        self.pointy = []
        self.pointz = []
        self.bodies = []
    def OnReportContact(self,vA,vB,cA,dist,rad,force,torque,modA,modB):
        bodyUpA = chrono.CastContactableToChBody(modA)
        nameA = bodyUpA.GetId()
        bodyUpB = chrono.CastContactableToChBody(modB)
        nameB = bodyUpB.GetId()
        self.pointx.append(vA.x)
        self.pointy.append(vA.y)
        self.pointz.append(vA.z)
        self.Fcx.append(force.x)
        self.Fcy.append(force.y)
        self.Fcz.append(force.z)
        self.bodies.append([nameA,nameB])
        return True        # return False to stop reporting contacts

    # reset after every run 
    def ResetList(self):
        self.pointx = []
        self.pointy = []
        self.pointz = [] 
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
        self.bodies=[]
    # Get the points
    def GetList(self):
        return (self.pointx, self.pointy, self.pointz, self.Fcx, self.Fcy, self.Fcz, self.bodies)
    

def Material(mu_f,mu_b,C,Ct,Cr,Cs):
    material = chrono.ChMaterialSurfaceNSC()
    material.SetFriction(mu_f)
    material.SetDampingF(mu_b)
    material.SetCompliance (C)
    material.SetComplianceT(Ct)
    # material.SetRollingFriction(mu_r) #Removing for now
    # material.SetSpinningFriction(mu_s)
    material.SetComplianceRolling(Cr)
    material.SetComplianceSpinning(Cs)
    return material

def Floor(material, length, tall, width=None, x_pos=0, z_pos=0, r=0, g=0, b=0):
    if width==None:
        width=length
    
    body_floor = chrono.ChBody()
    body_floor.SetBodyFixed(True)
    body_floor.SetPos(chrono.ChVectorD(x_pos, -tall, z_pos))
    body_floor.SetMaterialSurface(material)
    body_floor.GetCollisionModel().ClearModel()
    body_floor.GetCollisionModel().AddBox(length, tall, width) # hemi sizes
    body_floor.GetCollisionModel().BuildModel()
    body_floor.SetCollide(True)
    body_floor_shape = chrono.ChBoxShape()
    body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(length, tall, width)
    body_floor.GetAssets().push_back(body_floor_shape)
    black = chrono.ChColorAsset()
    black.SetColor(chrono.ChColor(r,g,b))
    body_floor.AddAsset(black)
    body_floor.SetId(0)
    return(body_floor)

def Wall(material, length, width, height, x_pos, z_pos, color, ID):
    """
    length: X-distance
    width: Z-distance
    height: Y-distance
    """
    wall = chrono.ChBodyEasyBox(length, height, width, True, True)
    wall.SetPos(chrono.ChVectorD(x_pos, height/2, z_pos))
    wall.SetMaterialSurface(material)
    wall.SetCollide(True)
    wall.SetBodyFixed(True)
    wall.SetId(ID)
    wall.AddAsset(color)
    return wall

class Poisson_Sampling:
    def __init__(self, min_distance, width, height):
        """
        Inputs:
            min_distance := The minimum distance between obstacles
            width := x_pos in chrono environment
            height := z_pos in chrono environment
        """

        self.k = 30

        # Minimum distance between samples
        self.r = min_distance

        self.width, self.height = width, height

        # Cell side length
        self.a = self.r/np.sqrt(2)
        # Number of cells in the x- and y-directions of the grid
        self.nx, self.ny = int(width / self.a) + 1, int(height / self.a) + 1

        # A list of coordinates in the grid of cells
        self.coords_list = [(ix, iy) for ix in range(self.nx) for iy in range(self.ny)]
        # Initilalize the dictionary of cells: each key is a cell's coordinates, the
        # corresponding value is the index of that cell's point's coordinates in the
        # samples list (or None if the cell is empty).
        self.cells = {coords: None for coords in self.coords_list}

    def get_cell_coords(self, pt):
        """Get the coordinates of the cell that pt = (x,y) falls in."""

        return int(pt[0] // self.a), int(pt[1] // self.a)

    def get_neighbours(self, coords):
        """Return the indexes of points in cells neighbouring cell at coords.

        For the cell at coords = (x,y), return the indexes of points in the cells
        with neighbouring coordinates illustrated below: ie those cells that could 
        contain points closer than r.

                                     ooo
                                    ooooo
                                    ooXoo
                                    ooooo
                                     ooo

        """

        dxdy = [(-1,-2),(0,-2),(1,-2),(-2,-1),(-1,-1),(0,-1),(1,-1),(2,-1),
            (-2,0),(-1,0),(1,0),(2,0),(-2,1),(-1,1),(0,1),(1,1),(2,1),
            (-1,2),(0,2),(1,2),(0,0)]
        neighbours = []
        for dx, dy in dxdy:
            neighbour_coords = coords[0] + dx, coords[1] + dy
            if not (0 <= neighbour_coords[0] < self.nx and
                    0 <= neighbour_coords[1] < self.ny):
                # We're off the grid: no neighbours here.
                continue
            neighbour_cell = self.cells[neighbour_coords]
            if neighbour_cell is not None:
                # This cell is occupied: store this index of the contained point.
                neighbours.append(neighbour_cell)
        return neighbours

    def point_valid(self, pt):
        """Is pt a valid point to emit as a sample?

        It must be no closer than r from any other point: check the cells in its
        immediate neighbourhood.

        """

        cell_coords = self.get_cell_coords(pt)
        for idx in self.get_neighbours(cell_coords):
            nearby_pt = self.samples[idx]
            # Squared distance between or candidate point, pt, and this nearby_pt.
            distance2 = (nearby_pt[0]-pt[0])**2 + (nearby_pt[1]-pt[1])**2
            if distance2 < self.r**2:
                # The points are too close, so pt is not a candidate.
                return False
        # All points tested: if we're here, pt is valid
        return True

    def get_point(self, k, refpt):
        """Try to find a candidate point relative to refpt to emit in the sample.

        We draw up to k points from the annulus of inner radius r, outer radius 2r
        around the reference point, refpt. If none of them are suitable (because
        they're too close to existing points in the sample), return False.
        Otherwise, return the pt.

        """
        i = 0
        while i < k:
            rho, theta = np.random.uniform(self.r, 2*self.r), np.random.uniform(0, 2*np.pi)
            pt = refpt[0] + rho*np.cos(theta), refpt[1] + rho*np.sin(theta)
            if not (0 <= pt[0] < self.width and 0 <= pt[1] < self.height):
                # This point falls outside the domain, so try again.
                continue
            if self.point_valid(pt):
                return pt
            i += 1
        # We failed to find a suitable point in the vicinity of refpt.
        return False

    def get_samples(self):
        # Pick a random point to start with.
        pt = (np.random.uniform(0, self.width), np.random.uniform(0, self.height))
        self.samples = [pt]
        # Our first sample is indexed at 0 in the samples list...
        self.cells[self.get_cell_coords(pt)] = 0
        # ... and it is active, in the sense that we're going to look for more points
        # in its neighbourhood.
        active = [0]

        nsamples = 1
        # As long as there are points in the active list, keep trying to find samples.
        while active:
            # choose a random "reference" point from the active list.
            idx = np.random.choice(active)
            refpt = self.samples[idx]
            # Try to pick a new point relative to the reference point.
            pt = self.get_point(self.k, refpt)
            if pt:
                # Point pt is valid: add it to the samples list and mark it as active
                self.samples.append(pt)
                nsamples += 1
                active.append(len(self.samples)-1)
                self.cells[self.get_cell_coords(pt)] = len(self.samples) - 1
            else:
                # We had to give up looking for valid points near refpt, so remove it
                # from the list of "active" points.
                active.remove(idx)
        return self.samples
    
def quadrant(x,z):
    """
    Takes a coordinate pair and determines which quadrant the coordiante pair are in.
    
    Note, this is meant to mimic the quadrants in chrono, not the actual careision plane. The positive z-direction is DOWN in videos
    
    Quad=0 indicates no contact on that bot
    """
    if x>0 and z>0:
        quad=4
    elif x<0 and z>0:
        quad=3
    elif x<0 and z<0:
        quad=2
    elif x>0 and z<0:
        quad=1
    return(quad)

