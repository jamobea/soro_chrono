import cv2
import numpy as np
from FourierController import nufft, ifft_n
from PIL import Image, ImageDraw

def image2Points(filename):
    # Open file
    im = cv2.imread(filename)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    im = cv2.flip(im, 0)

    # Generate points
    p, hierarchy = cv2.findContours(
        im, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    p = np.reshape(p[1], (p[1].shape[0], 2))

    return p

class FourierTargetGeo:

    # Positions of boundary
    pos = np.array([])
    ipos = np.array([]) # Initial positions (original)
    vel = np.array([])

    # Positions of
    ffpos = np.array([])
    ffvel = np.array([])
    order = -1

    scale = 1
    units = None
    imagesize=(0, 0)

    def __init__(self, geo, order, scale=1, imagesize=(2, 2), units=None, pcm_t=None, vcm_t=None):

        if type(geo) == str:
            ipos = image2Points(geo)
        else:
            ipos = geo

        self.ipos = ipos
        self.order = order
        self.scale = scale


        self.imagesize = imagesize
        self.originimage = np.array(self.imagesize) / 2
        self.units = units

        self.redefinePoints()

        self.applyInitialTransform()

    def redefinePoints(self):
        # Center geometry
        p_tmp = np.subtract(self.ipos, np.average(self.ipos, axis=0))
        m = np.max(np.sqrt(np.sum(p_tmp ** 2, axis=1)))

        # Normalize size
        p_tmp = p_tmp / m / 2
        self.ipos = p_tmp * self.scale

        # Create nonuniform fourier transform
        # variables needed for transform
        dxy = np.diff(np.vstack((self.ipos, self.ipos[0, :])), axis=0)
        ds = np.sqrt(np.sum(dxy ** 2, axis=1))
        s = np.concatenate(([0], np.cumsum(ds)))[:-1]

        pc = self.ipos[:, 0] + 1j * self.ipos[:, 1]

        # Store fourier result and resampled data
        self.ffpos = nufft(pc, self.order, ds[:-1], s)
        self.pos = ifft_n(self.ffpos, num_points=max(self.ipos.shape))

        self.ffvel = np.zeros(self.ffpos.shape)
        self.vel = np.zeros(self.pos.shape)

    def applyInitialTransform(self):

        self.ffpos *= np.exp(1j * 90 * np.pi / 180)
        self.pos = ifft_n(self.ffpos, num_points=max(self.ipos.shape))


    def getImage(self):
        pos_tmp = np.vstack((np.real(self.pos), np.imag(self.pos)))

        if self.units == None:
            img = np.ones((int(self.imagesize[1]), int(self.imagesize[0]), 3), np.uint8) * 200
        else:
            w = int(self.units.Meters2Pixels(self.imagesize[1]))
            h = int(self.units.Meters2Pixels(self.imagesize[0]))
            img = np.ones((w, h, 3), np.uint8) * 200

        sk = self.units.Meters2Pixels(1)
        points = sk * ((np.array([[1, 0], [0, -1]]) @ pos_tmp).T + self.originimage)

        points = points.reshape(-1, 1, 2)
        points = points.astype(int)

        cols = np.ones(max(points.shape))
        for i, c in enumerate(cols):
            p1 = tuple(points[i, 0, :])
            p2 = tuple(points[(i+1) % points.shape[0], 0, :])

            if i == 0 or i == len(cols) - 1:
                img = cv2.line(img, p1, p2, (255, 255, 255), 3)
            else:
                img = cv2.line(img, p1, p2, 0*255, 3)



        return Image.fromarray(img)