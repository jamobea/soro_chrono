import numpy as np
from numpy import array, block
import cvxopt
from scipy.linalg import block_diag, sqrtm
from threading import Thread
import matplotlib.pyplot as plt


# minimize u for:  0.5 * u.T @ P @ u + q.T @ u
# subject to:            Aineq @ u <= bineq
#                          Aeq @ u  = beq
def cvxopt_solve_qp(P, q, Aineq=None, bineq=None, Aeq=None, beq=None):
    P = .5 * (P + P.T)  # make sure P is symmetric
    args = [cvxopt.matrix(P), cvxopt.matrix(q)]
    if Aineq is not None:
        args.extend([cvxopt.matrix(Aineq), cvxopt.matrix(bineq)])
        if Aeq is not None:
            args.extend([cvxopt.matrix(Aeq), cvxopt.matrix(beq)])
    cvxopt.solvers.options['show_progress'] = False
    sol = cvxopt.solvers.qp(*args)
    if 'optimal' not in sol['status']:
        return None
    return np.array(sol['x']).reshape((P.shape[1],))


# Inverse fourier transform, use num_points, complex version (asumes series is of length 2N + 1)
def ifft_n(series, num_points=360):
    series = np.fft.fftshift(series)
    if num_points > len(series):
        fft_data = np.fft.ifftshift(
            np.concatenate((np.zeros((num_points - len(series)) // 2+ (num_points+1) % 2), series,
                            np.zeros((num_points - len(series)) // 2))))
    elif num_points < len(series):
        k = (len(series) - num_points)//2
        i = (len(series) - num_points) % 2
        series = series[k:len(series)-k-i]
        fft_data = np.fft.ifftshift(series)
    else:
        fft_data = np.fft.ifftshift(series)

    new_points = np.fft.ifft(fft_data) * num_points

    return new_points

# Inverse fourier transform, use num_points, real version (asumes series is of length N + 1)
def irfft_n(series, num_points=360):

    if num_points//2 + 1 > len(series):
        n = num_points//2 + 1
        n -= len(series)
        fft_data = np.fft.irfft(np.concatenate((series, np.zeros((n)))), num_points)
    elif num_points//2 + 1 < len(series):
        k = num_points//2 + 1
        series = series[:k]
        fft_data = np.fft.irfft(series, num_points)
    else:
        fft_data = np.fft.irfft(series, num_points)

    new_points = fft_data * num_points

    return new_points

# Calculate non-uniform fft of order 10, complex version, will return length 2N + 1
def nufft(data, order=10, dist_dt=None, dist_t=None):
    dxy = np.diff(data)
    if dist_dt is None and dist_t is None:
        # dxy = np.diff(data[:, 1])
        # dt = np.diff(data[:, 0])
        # t = np.concatenate([([0.0]), np.cumsum(dt)])
        # T = t[-1]  #
        t = np.linspace(0, 1, len(data))
        dt = np.ones(len(data)-1) / len(data)
        T = 1
    else:
        dt = dist_dt
        t = dist_t
        T = dist_t[-1]

    phi = (2. * np.pi * t) / T

    orders = np.arange(1, order + 1)
    consts = T / (2. * orders * orders * np.pi * np.pi)
    phi = phi * orders.reshape((order, -1))
    d_cos_phi = np.cos(phi[:, 1:]) - np.cos(phi[:, :-1])
    d_sin_phi = np.sin(phi[:, 1:]) - np.sin(phi[:, :-1])

    a = consts * np.sum((dxy / dt) * d_cos_phi, axis=1)
    b = consts * np.sum((dxy / dt) * d_sin_phi, axis=1)

    dc = np.trapz(data, t) / T
    c_pos = (a - 1j * b) / 2
    c_neg = np.flip((a + 1j * b) / 2)
    cn = np.hstack((c_neg, [dc], c_pos))

    return np.fft.ifftshift(cn)

# Calculate non-uniform fft of order 10, real version, will return length N + 1
def nurfft(data, order=10, dist_dt=None, dist_t=None):
    dxy = np.diff(data)
    if dist_dt is None and dist_t is None:
        # dxy = np.diff(data[:, 1])
        # dt = np.diff(data[:, 0])
        # t = np.concatenate([([0.0]), np.cumsum(dt)])
        # T = t[-1]  #
        t = np.linspace(0, 1, len(data))
        dt = np.ones(len(data)-1) / len(data)
        T = 1
    else:
        dt = dist_dt
        t = dist_t
        T = dist_t[-1]

    phi = (2. * np.pi * t) / T

    orders = np.arange(1, order + 1)
    consts = T / (2. * orders * orders * np.pi * np.pi)
    phi = phi * orders.reshape((order, -1))
    d_cos_phi = np.cos(phi[:, 1:]) - np.cos(phi[:, :-1])
    d_sin_phi = np.sin(phi[:, 1:]) - np.sin(phi[:, :-1])

    a = consts * np.sum((dxy / dt) * d_cos_phi, axis=1)
    b = consts * np.sum((dxy / dt) * d_sin_phi, axis=1)

    dc = np.trapz(data, t) / T
    c_pos = (a - 1j * b) / 2
    cn = np.hstack(([dc], c_pos))

    return cn

def ifft_psi(series, s):

    #s = s / s[-1] * 2 * np.pi
    scl = len(s) / len(series)
    num_points = len(s)

    v1 = 2j * np.pi * np.arange(num_points)
    v2 = s / 2 / np.pi

    mat = np.exp(np.outer(v1, v2))

    series = np.fft.fftshift(series)
    if num_points > len(series):
        series = np.concatenate((np.zeros((num_points - len(series)) // 2 + (num_points + 1) % 2), series,
                            np.zeros((num_points - len(series)) // 2)))
    elif num_points < len(series):
        k = (len(series) - num_points) // 2
        i = (len(series) - num_points) % 2
        series = series[k:len(series) - k - i]

    return mat.T @ np.fft.ifftshift(series)

# Base class to define controller based on fourier descriptors, in its base form it is a PD controller
class FourierController:

    # constants that are useful
    numBots = -1        # Number of bots
    order = -1         # Order of fourier descriptor
    max_u = -1          # Maximum control actuation
    dt = 0.01      # Timestep

    rho = 1
    b_drag = 0

    # variables to update on each timestep
    pos = array([])      # current position
    vel = array([])     # current velocity
    norm = array([])        # current normals
    avel = array([])

    # Fourier variables
    ffpos = array([])
    ffvel = array([])
    ffnorm = array([])
    ffavel = array([])

    target = None

    # parametric curve variables
    ds = array([])          # differential of s
    s = array([])           # list of s values
    S = 1                   # total length of S parameter

    # Flag to enable thread acceleration
    enable_threading = False

    calculating = False

    # Constructor:
    # numBots - number of bots
    # order   - order of descriptors
    # max_u   - maximum actuation force (box constraint)
    # dt      - time-step duration
    # rest_l  - resting length
    # rho     - density of membrane (mass of bots)
    # b       - drag coefficient
    def __init__(self, numBots, order, max_u=-1, dt=.01, rest_l=1, rho=1, b_drag=0):
        self.numBots = numBots
        self.order = order
        self.max_u = max_u
        self.dt = dt
        self.rest_l = rest_l
        self.rho = rho
        self.b_drag = b_drag

        self.output = np.zeros((self.numBots, 2))

        self.createStaticVariables()

    def createStaticVariables(self):
        pass

    # Convert to QP prblem:
    # INPUT:
    #      A, B   - System matrices
    #      Q, R   - Weight matrices (for position error and actuation effort)
    #         P   - Final position error weight
    #         N   - Number of steps in horizon
    # OUTPUT:
    #      krhc   - Feedback control matrix [u = krhc * x_k, given x_k+1 = A * x_k + B * u = (A + krhc) * x_k]
    #      F, G   - J = .5 * U' * G * U + U' * F * X0
    # phi, gamm   - X_bar = phi * X0 + gamm * U  (converts list uf U to list of X)
    def getMPCMatrices(self, A, B, Q, R, P, N):
        m = np.array(B).shape[0] # State size
        n = np.array(B).shape[1] # Control size

        # First the matrices which allow x_bar to be calculated form x0 and u_bar:
        try:
            omega = np.kron(np.eye(N - 1), Q)
            omega = block_diag(omega, P)
        except:
            omega = P

        psi = np.kron(np.eye(N), R)

        # Now the matrices for the QP solver:
        phi = [[]]
        gamm = [[]]
        A_prev = np.eye(m)
        tmp = np.zeros((m * N, n))
        for i in range(N):
            tmp = np.vstack((tmp[m:, :], A_prev @ B))

            try:
                gamm = np.hstack((tmp, gamm))
            except:
                gamm = tmp

            A_prev = A_prev @ A

            try:
                phi = np.vstack((phi, A_prev))
            except:
                phi = A_prev

        # test = rho' * omega * rho
        G = 2 * (psi + gamm.T @ omega @ gamm)
        G = (G + G.T)/2            # To ensure matlab will consider G symmetric
        F = 2 * gamm.T @ omega @ phi

        mask = np.hstack((np.eye(n), np.zeros((n, n * (N - 1)))))

        krhc = - mask @ np.linalg.inv(G) @ F

        return krhc, F, G, phi, gamm, mask

    # Used to generate box constraints of the MPC problem (not working now)
    def getMPCConstraintMatrices(self, C, phi, rho, N, xmax=None, xmin=None, ymax=None, ymin=None):

        # Create matrix to convert X_bar to Y_bar
        Cmat = np.kron(np.eye(N), C)

        Aineq = np.empty((0, rho.shape[1]), float)
        bineq_c = np.empty((0, N), float)
        bineq_x0 = np.empty((0, phi.shape[1]), float)


        # if xmax != None:
        #     Aineq = np.vstack((Aineq, rho))
        #     bineq_c = np.vstack((bineq_c, np.kron(np.ones(N, 1), xmax)))
        #     bineq_x0 = np.vstack((bineq_x0, - phi))
        #
        # if xmin != None:
        #     Aineq = np.vstack((Aineq, - rho))
        #     bineq_c = np.vstack((bineq_c, - np.kron(np.ones(N, 1), xmin)))
        #     bineq_x0 = np.vstack((bineq_x0, phi))
        #
        # if ymax != None:
        #     Aineq = np.vstack((Aineq, Cmat @ rho))
        #     bineq_c = np.vstack((bineq_c, np.kron(np.ones(N, 1), ymax)))
        #     bineq_x0 = np.vstack((bineq_x0, - Cmat @ phi))
        #
        # if ymin != None:
        #     Aineq = np.vstack((Aineq, - Cmat @ rho))
        #     bineq_c = np.vstack((bineq_c, - np.kron(np.ones(N, 1), ymin)))
        #     bineq_x0 = np.vstack((bineq_x0, Cmat @ phi))

        return Aineq, bineq_c, bineq_x0

    # Usually function called externally, updates state information and reference values as well as calls
    # calculateControl. Returns the U vector for that time-step
    def update(self, positions, velocities, normals, ang_vel):
        self.calculating = True

        # Update state data
        self.pos = positions.T.flatten()
        self.vel = velocities.T.flatten()

        self.norm = normals.T.flatten()
        self.avel = ang_vel


        # Launch controller calculation now
        if self.enable_threading:
            self.thread = Thread(target=self.startFourierConversionThread)
            self.thread.start()

            self.thread = Thread(target=self.startControlCalculationThread)
            self.thread.start()
        else:
            self.startFourierConversionThread()
            self.startControlCalculationThread()

    # Waits for the given thread to finish
    def waitForThread(self):
        self.thread.join()

    # Thread to convert pose to fourier domain
    def startFourierConversionThread(self):
        # Calculate FFTs
        p_tmp = np.reshape(self.pos, (self.numBots, 2))  # - np.array(self.ref.originimage) / self.ref.scale
        v_tmp = np.reshape(self.vel, (self.numBots, 2))
        n_tmp = np.reshape(self.norm, (self.numBots, 2))

        # Calculate spread:
        dxy = np.diff(np.vstack((p_tmp, p_tmp[0, :])), axis=0)
        self.ds = np.sqrt(np.sum(dxy ** 2, axis=1))
        self.s = np.concatenate(([0], np.cumsum(self.ds)))[:-1]
        self.S = self.s[-1]

        self.ffpos = nufft(p_tmp[:, 0] + 1j * p_tmp[:, 1], self.order, self.ds[:-1], self.s)
        self.ffvel = nufft(v_tmp[:, 0] + 1j * v_tmp[:, 1], self.order, self.ds[:-1], self.s)
        self.ffnorm = nufft(n_tmp[:, 0] + 1j * n_tmp[:, 1], self.order, self.ds[:-1], self.s)

    # Thread to calculate control action (calls 'calculateStaticVariables' function)
    def startControlCalculationThread(self):
        # Run optimization routine
        out = self.calculateControl()

        # Make sure the output is within the allowed limits, if the MPC is calculated correctly this shouldn't change
        # anything
        # if self.max_c > 0:
        #     out[out > self.max_c] = self.max_c
        #     out[out < - self.max_c] = - self.max_c

        self.output = out

        self.calculating = False

    def calculateStaticVariables(self):

        # ToDo: implement function and erase this line:
        pass

    def calculateControl(self):
        # For now apply PID if target geo is available:
        x = np.hstack((self.ffpos, self.ffvel))

        if self.target == None:
            u = np.zeros(self.order * 2 + 1, dtype=complex)
        else:
            xdes = np.hstack((self.target.ffpos, self.target.ffvel))

            u = self.controlLaw(x, xdes)

        us1 = ifft_n(u, self.numBots)
        us1 = np.vstack((np.real(us1), np.imag(us1))).T
        us = ifft_psi(u, self.s)
        us = np.vstack((np.real(us), np.imag(us))).T

        #plt.plot(us1[:, 0])
        #plt.plot(us1[:, 1])
        #plt.show()

        return us1

    def controlLaw(self, x, x_des):
        mid = self.order * 2 + 1
        x_diff = x - x_des
        u = -.05 * x_diff[:mid] - .04 * x_diff[mid:]

        return u

class MPC_Freeform_Controller(FourierController):

    def createStaticVariables(self):
        kn = self.order * 2 + 1
        I_mat = np.eye(kn)
        z_mat = I_mat * 0

        # Define state matrices
        A = block([[z_mat, I_mat], [z_mat, -self.b_drag / self.rho * I_mat]])
        B = block([[z_mat], [1 / self.rho * I_mat]])

        Ad = np.eye(kn * 2) + self.dt * A
        Bd = self.dt * B

        # Controller tuning:
        w_p = 1             # Position weight
        w_v = .3             # Velocity weight
        w_u = .1             # Controller weight
        w_f = 3            # Final cost weight (multiplies)
        fade = 5.6           # distribution: .001 -> only position is taken into account
                            #               0.45 -> linear decay
                            #                  5 -> all equal


        curve = lambda n, fade: np.exp(-.5 * n ** 2 / fade ** 2 / self.order ** 2)
        idx = np.arange(0, self.order + 1)
        coeffs = curve(np.concatenate((idx, np.flip(idx[1:]))), fade)

        Q = block_diag(w_p * np.diag(coeffs), w_v * np.diag(coeffs))
        P = Q * w_f
        R = w_u * np.diag(coeffs)
        N = 10

        krhc, F, G, phi, gamm, mask = self.getMPCMatrices(Ad, Bd, Q, R, P, N)

        self.krhc = krhc

    def controlLaw(self, x, x_des):
        mid = self.order * 2 + 1
        x_diff = x - x_des
        u = self.krhc @ x_diff

        return u

class MPC_Constrained_Freeform_Controller(FourierController):

    def createConstraints(self):
        # Define constraint matrices
        K = self.order * 2 + 1
        n = np.arange(0, K, 1)
        k = np.fft.ifftshift(np.arange(-(K // 2), K // 2 + (K % 2), 1))

        angles = 2 * np.pi * np.outer(n, k) / K
        cos_mat = np.cos(angles)
        sin_mat = np.sin(angles)
        real_mat = np.hstack((np.kron(np.eye(self.N), cos_mat), -np.kron(np.eye(self.N), sin_mat)))
        real_mat = np.vstack((real_mat, -real_mat))
        imag_mat = np.hstack((np.kron(np.eye(self.N), sin_mat), np.kron(np.eye(self.N), cos_mat)))
        imag_mat = np.vstack((imag_mat, -imag_mat))
        full_mat = np.vstack((real_mat, imag_mat))

        self.Aineq = full_mat
        self.bineq = np.ones((K * self.N * 4, 1)) * self.max_u

    def createStaticVariables(self):
        kn = self.order * 2 + 1
        I_mat = np.eye(kn)
        z_mat = I_mat * 0

        # Define state matrices
        A = block([[z_mat, I_mat], [z_mat, -self.b_drag / self.rho * I_mat]])
        B = block([[z_mat], [1 / self.rho * I_mat]])

        Ad = np.eye(kn * 2) + self.dt * A
        Bd = self.dt * B

        # Augment matrices to include reference
        Aa = block_diag(Ad, Ad)  # np.eye(A.shape[0]))
        Ba = np.block([[Bd], [np.zeros(Bd.shape)]])
        Ca = np.block([[np.eye(Ad.shape[0]), -np.eye(Ad.shape[0])]])

        # Controller tuning:
        w_p = 1  # Position weight
        w_v = .5  # Velocity weight
        w_u = 2  # Controller weight
        w_f = 1  # Final cost weight (multiplies)
        fade = .6  # distribution: .001 -> only position is taken into account
        #               0.45 -> linear decay
        #                  5 -> all equal

        curve = lambda n, fade: np.exp(-.5 * n ** 2 / fade ** 2 / self.order ** 2)
        idx = np.arange(0, self.order + 1)
        coeffs = curve(np.concatenate((idx, np.flip(idx[1:]))), fade)

        Q = block_diag(w_p * np.diag(coeffs), w_v * np.diag(coeffs))
        P = Q * w_f
        R = w_u * np.diag(coeffs)
        self.N = 5

        Qa = Ca.T @ Q @ Ca
        Pa = Ca.T @ P @ Ca

        krhc, self.F, self.G, phi, gamm, self.mask = self.getMPCMatrices(Aa, Ba, Qa, R, Pa, self.N)

        self.EE = block_diag(self.G, self.G)

        self.krhc = krhc

        self.createConstraints()

    def controlLaw(self, x, x_des):

        xi_a = np.concatenate((x, x_des))

        tmp = self.F @ xi_a
        q = 2 * np.hstack((np.real(tmp), np.imag(tmp)))
        out = cvxopt_solve_qp(self.EE, q, self.Aineq, self.bineq)

        u = np.block([np.eye((self.order * 2 + 1) * self.N), 1j * np.eye((self.order * 2 + 1) * self.N)]) @ out

        return self.mask @ u


class MPC_NormalConstrained_Freeform_controller(MPC_Constrained_Freeform_Controller):

    def createConstraints(self):
        # Define constraint matrices
        K = self.order * 2 + 1
        n = np.arange(0, K, 1)
        k = np.fft.ifftshift(np.arange(-(K // 2), K // 2 + (K % 2), 1))

        angles = 2 * np.pi * np.outer(n, k) / K
        cos_mat = np.cos(angles)
        sin_mat = np.sin(angles)
        real_mat = np.hstack((np.kron(np.eye(self.N), cos_mat), -np.kron(np.eye(self.N), sin_mat)))
        real_mat2 = np.vstack((real_mat, -real_mat))
        imag_mat = np.hstack((np.kron(np.eye(self.N), sin_mat), np.kron(np.eye(self.N), cos_mat)))
        imag_mat2 = np.vstack((imag_mat, -imag_mat))

        full_mat = np.vstack((real_mat2, imag_mat2))

        self.Aineq = full_mat
        self.bineq = np.ones((K * self.N * 4, 1)) * self.max_u





    def controlLaw(self, x, x_des):

        xi_a = np.concatenate((x, x_des))

        tmp = self.F @ xi_a
        q = 2 * np.hstack((np.real(tmp), np.imag(tmp)))
        out = cvxopt_solve_qp(self.EE, q, self.Aineq, self.bineq)

        u = np.block([np.eye((self.order * 2 + 1) * self.N), 1j * np.eye((self.order * 2 + 1) * self.N)]) @ out

        return self.mask @ u